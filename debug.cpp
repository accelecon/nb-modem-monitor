//--------------------------------------------------------------------------
// Module: Debug Log                                          +-----------+
// Author: Michael Nagy                                       | debug.cpp |
// Date:   15-Aug-2012                                        +-----------+
//--------------------------------------------------------------------------

#include "main.h"

#define FMT_SYSLOG(x) va_list args; va_start( args, fmt); vsyslog( x, fmt, args); va_end( args)

void ErrorLogLn( ccptr fmt, ... ) {
  FMT_SYSLOG( LOG_ERR);
}

void DebugLogLn( ccptr fmt, ... ) {
  FMT_SYSLOG( LOG_DEBUG);
}

void QmodemLogLn( ccptr fmt, ... ) {
  FMT_SYSLOG( LOG_DEBUG);
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

