//--------------------------------------------------------------------------
// Module: Utility Functions                                     +--------+
// Author: Michael Nagy                                          | util.h |
// Date:   15-Aug-2012                                           +--------+
//--------------------------------------------------------------------------

void  UtilSleep   ( int  s);
void  UtilSleep_mS( int ms);
void  UtilSleep_uS( int us);

void UtilTimerReset( void);
uint32_t UtilTimer_mS( void);

void  UtilFileCreate( ccptr Filename);
bool  UtilFileExists( ccptr Filename);
void  UtilFileDelete( ccptr Filename);
ccptr UtilFileValue ( ccptr Filename);
bool  UtilFileSet   ( ccptr Flagname, ccptr Value);
void  UtilFlagSet   ( ccptr Flagname);
void  UtilFlagForce ( ccptr Flagname);

uint8_t UtilBits( uint8_t Value);
ccptr UtilTrim( ccptr String);
uint32_t UtilHexToBit32( ccptr Hex);
int UtilRangeCheck( int Min, int Max, int Default, int & Value);
ccptr UtilStrCpy( cptr Target, ccptr Source, int TargetSize);
int UtilStrCmp( ccptr String1, ccptr String2);

  int UtilStr2Int  ( ccptr String);
float UtilStr2Float( ccptr String);

bool UtilStrStr( ccptr String, ccptr Substring);

ccptr UtilInt2Str  (   int Value);
ccptr UtilFloat2Str( float Value);

ccptr UtilGetField( ccptr String, int Index, char Delimiter);
ccptr UtilPrefix( ccptr String, int Length);

bool  UtilStartsWith( ccptr String, ccptr Prefix);
ccptr  UtilUcase( ccptr String);
ccptr  UtilLcase( ccptr String);
ccptr  UtilDateTime( void);

bool UtilSystem( Reply_t & Reply, ccptr Command);
void UtilReplyFree( Reply_t & Reply);

// Minimal interface to cached dynamic strings implementation.

ccptr TempDup( ccptr String);
void TempFlush( void);

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
