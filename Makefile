#--------------------------------------------------------------------------
# Project: Modem Monitor                                      +----------+
# Author:  Michael Nagy                                       | Makefile |
# Date:    09-Aug-2012                                        +----------+
#--------------------------------------------------------------------------

all: modem-monitor

SOURCES  = main.cpp debug.cpp modem.cpp mutex.cpp qmodem.cpp serial.cpp util.cpp

INCLUDES = ${SOURCES:.cpp=.h}
OBJECTS  = ${SOURCES:.cpp=.o}

main.o:   main.cpp   $(INCLUDES)
debug.o:  debug.cpp  $(INCLUDES)
modem.o:  modem.cpp  $(INCLUDES)
mutex.o:  mutex.cpp  $(INCLUDES)
qmodem.o: qmodem.cpp $(INCLUDES)
serial.o: serial.cpp $(INCLUDES)
util.o:   util.cpp   $(INCLUDES)

modem-monitor:                   $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OBJECTS) -lusb -lrt -lpthread -lm -lcurl -lroxml -o modem-monitor

.PHONY: clean
clean:
	@rm -f modem-monitor *.o *~

#--------------------------------------------------------------------------
# End
#--------------------------------------------------------------------------

