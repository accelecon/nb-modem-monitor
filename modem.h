//--------------------------------------------------------------------------
// Module: Modem Interface                                      +---------+
// Author: Michael Nagy                                         | modem.h |
// Date:   10-Aug-2012                                          +---------+
//--------------------------------------------------------------------------

// ModemQueryAlloc() is non-reentrant, and may block if another process has
// invoked the scmd utility (from which the mutex and logic were cloned).
// You may re-use a Reply pointer for multiple calls to ModemQueryAlloc();
// it will be freed and reallocated as appropriate.  If ModemQueryAlloc()
// returns false, it will also set Reply to NULL.  You must eventually
// call ModemQueryFree() when you are done using the Reply data.  It is
// safe to call ModemQueryFree() with a NULL pointer.

bool ModemQueryAlloc( Reply_t & Reply, ccptr UsbPort, ccptr Command);
bool ModemQmiQueryAlloc( Reply_t & Reply, ccptr WdmPort, ccptr Command);
void ModemQueryFree ( Reply_t & Reply);

// The ModemGetValue routines return NULL or pointers to ASCIIZ strings.

ccptr ModemGetValue     ( Reply_t Text, ccptr Key, bool ToEndOfLine = false);
ccptr ModemGetValueIndex( Reply_t Text, ccptr Key, int Index);
ccptr ModemGetValueRaw  ( Reply_t Text);

ccptr ModemRemapCssBand( Reply_t Text);
ccptr ModemRemapCssSid ( Reply_t Text);
ccptr ModemRemapCssNid ( Reply_t Text);

ccptr ModemRemapHdrNetwork( cptr StatusHdrRevision);
ccptr ModemRemapCntiNetwork( cptr Cnti, cptr Cops, cptr RelProtocol);

void ModemGetLacCidGsm( Reply_t Creg, Reply_t Cgreg, ccptr & Lac, ccptr & Cid);

bool ModemFindString( Reply_t Response, ccptr Target);

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
