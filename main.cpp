//--------------------------------------------------------------------------
// Module: Modem Monitor and Display Utility                   +----------+
// Author: Michael Nagy                                        | main.cpp |
// Date:   17-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

// This utility is specific to NetBridge, and is launched at startup to
// monitor the signal strength and network status of the active
// modem (which may change over time) and display the results on the
// signal and 3g leds.

#include "main.h"
int clean_exit = 0;

// ActiveModem is an ASCIIZ string which identifies the currently-
// active/selected modem by its short-name (i.e. 'sw308').  It will be zero-
// length if there is no currently-active/selected modem.  Its value may
// also change over time, so it is volatile.  The ActiveVid/Pid/Type/Port
// values will track ActiveModem.

       char ActiveModem[16] = "";
       char ActiveVid  [ 8] = "";
       char ActivePid  [ 8] = "";
       char ActiveType [16] = "";
static char ActivePort [32] = "";

struct DataBuf {
  char * data;
  size_t size;
};

// Curl cals this function, which is expected to read the incoming HTTP data
size_t SaveHttpData(char *ptr, size_t size, size_t nmemb, void *userdata){
  DataBuf * db = (DataBuf *) userdata;
  size_t InputSize = nmemb * size;
  // The size of our buffer is fixed, so we need to limit our copy to that.
  // Leave room at the end for a null (roxml won't take a length arguement, so
  // this has to be NULL-terminated) even though we know the size.
  size_t MemCpSize = (InputSize < db->size - 1) ? InputSize : db->size - 1;
  memcpy(db->data, ptr, MemCpSize);
  db->data[MemCpSize] = NULL;
  db->size = MemCpSize;
  // If we don't tell curl we copied it all, it thinks there was an error.
  return InputSize;
}

// Refresh active modem and related values.  If logging is enabled and they
// have not been previously logged, log them now.

static void RefreshActiveModem( void) {
  static bool PreviouslyLogged = false;
  char Temp[32];
  UtilStrCpy( Temp, UtilFileValue( TMP_VALUE_MODEM), sizeof( Temp));
  if ((PreviouslyLogged == false) || UtilStrCmp( ActiveModem, Temp)) {
    UtilStrCpy( ActiveModem, Temp, sizeof( ActiveModem));
    DebugLogLn( "ActiveModem '%s'", ActiveModem);
  }
  UtilStrCpy( Temp, UtilFileValue( TMP_VALUE_MODEM_SCMD), sizeof( Temp));
  if ((PreviouslyLogged == false) || UtilStrCmp( ActivePort, Temp)) {
    UtilStrCpy( ActivePort, Temp, sizeof( ActivePort));
    DebugLogLn( "ActivePort '%s'", ActivePort);
  }
  UtilStrCpy( Temp, UtilFileValue( TMP_VALUE_MODEM_VID), sizeof( Temp));
  if ((PreviouslyLogged == false) || UtilStrCmp( ActiveVid, Temp)) {
    UtilStrCpy( ActiveVid, Temp, sizeof( ActiveVid));
    DebugLogLn( "ActiveVid '%s'", ActiveVid);
  }
  UtilStrCpy( Temp, UtilFileValue( TMP_VALUE_MODEM_PID), sizeof( Temp));
  if ((PreviouslyLogged == false) || UtilStrCmp( ActivePid, Temp)) {
    UtilStrCpy( ActivePid, Temp, sizeof( ActivePid));
    DebugLogLn( "ActivePid '%s'", ActivePid);
  }
  UtilStrCpy( Temp, UtilFileValue( TMP_VALUE_MODEM_TYPE), sizeof( Temp));
  if ((PreviouslyLogged == false) || UtilStrCmp( ActiveType, Temp)) {
    UtilStrCpy( ActiveType, Temp, sizeof( ActiveType));
    DebugLogLn( "ActiveType '%s'", ActiveType);
  }
  PreviouslyLogged = true;
  TempFlush(); // must call once per main loop before any dynamic strings are used
}

// A handy macro

#define ModemQuery(Command) ModemQueryAlloc( Reply, ActivePort, Command)
#define ModemQmiQuery(Command) ModemQmiQueryAlloc( Reply, "/dev/cdc-wdm0", Command)

//--------------------------------------------------------------------------
// Query the network connection type of the active modem.  This routine is a
// functional replacement for the old query3g.sh script.
//--------------------------------------------------------------------------

static void Query3g( void) {
  Reply_t Reply = NULL;
  ccptr Cnti[8], Line;
  int CntiN = 0;
  ccptr Flag3g = "-";
  memset(Cnti, 0, sizeof(Cnti));
  if ( UtilFileExists( TMP_FLAG_FAMILY_QMI_)) {
    if (ModemQmiQuery( "--nas-get-serving-system")) {
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "Radio interfaces:")) {
          Line = UtilGetField( Line, 2, '\'');
          for (int j = UtilStr2Int(Line); j > 0; j--) {
            Line = Reply[i+j];
            DebugLogLn(Line);
            Cnti[CntiN] = UtilGetField( Line, 2, '\'');
            Cnti[CntiN] = UtilUcase( Cnti[CntiN]);
            CntiN++;
          }
  } } } } else {
    //family QMI may also be one of the others: skip AT commands if QMI
    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA1_)) {
      if (ModemQuery( "!STATUS")) {
        for (int i = 0; Line = Reply[i]; i++) {
          if (UtilStrStr( "Sys Mode", Line)) {
            Cnti[0] = UtilGetField( Line, 7, ' ');
    } } } }

    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA2_)) {
      if (ModemQuery( "$NWSYSMODE")) {
        Cnti[0] = ModemGetValueRaw( Reply);
    } }
    
    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA3_)) {
      if (ModemQuery( "$$SYS_SRV_MODE")) {
        switch( UtilStr2Int( UtilGetField( ModemGetValueRaw( Reply), 3, ':'))) {
          case 1: Cnti[0] = "IS-95"; break;
          case 2: Cnti[0] = "CDMA" ; break;
          case 3:
          case 4: Cnti[0] = "EV-DO"; break;
          default:
            if (UtilStrStr( "fw210,fw600", ActiveType)) {
              if (ModemQuery( "+SERVICE?")) {
                Cnti[0] = ModemGetValueRaw( Reply);
                switch (UtilStr2Int( Cnti[0])) {
                  case 1: Cnti[0] = "1xRTT"      ; break;
                  case 2: Cnti[0] = "EV-DO"      ; break;
                  case 3: Cnti[0] = "EV-DO Rev A"; break;
    } } }   } } }
    
    if (UtilFileExists( TMP_FLAG_WIMAX_CONNECTED)) {
      Cnti[0] = "WiMAX";
    }
    
    if (UtilFileExists( TMP_FLAG_FAMILY_GSM1_)) {
      if (UtilStrStr( "op461", ActiveType)) {
        if (ModemQuery( "_OWCTI?")) {
          Cnti[0] = UtilGetField( ModemGetValueRaw( Reply), 2, ':');
          switch (UtilStr2Int( Cnti[0])) {
            case 0: Cnti[0] = "EDGE"       ; break;
            case 1: Cnti[0] = "WCDMA"      ; break;
            case 2: Cnti[0] = "HSDPA"      ; break;
            case 3: Cnti[0] = "HSUPA"      ; break;
            case 4: Cnti[0] = "HSDPA/HSUPA"; break;
        } }
      } else {
        if (ModemQuery( "*CNTI=0")) {
          Cnti[0] = UtilGetField( ModemGetValueRaw( Reply), 2, ',');
    } } }
    
    if (UtilFileExists( TMP_FLAG_FAMILY_GSM2_) ||
        UtilFileExists( TMP_FLAG_FAMILY_GSM3_)) {
      if (ModemQuery( "+COPS?")) {
        Cnti[0] = UtilGetField( ModemGetValueRaw( Reply), 4, ',');
        switch (UtilStr2Int( Cnti[0])) {
          case 0: Cnti[0] = "GSM"              ; break;
          case 1: Cnti[0] = "GSM-C"            ; break;
          case 2: Cnti[0] = "UTRAN"            ; break;
          case 3: Cnti[0] = "GSM+EGPRS"        ; break;
          case 4: Cnti[0] = "UTRAN+HSDPA"      ; break;
          case 5: Cnti[0] = "UTRAN+HSUPA"      ; break;
          case 6: Cnti[0] = "UTRAN+HSDPA/HSUPA"; break;
          case 7: Cnti[0] = "HSPA+"            ; break;
    } } }
    
    if (UtilStrStr( "zt683", ActiveType)) {
      if (ModemQuery( "+ZPAS?")) {
        Cnti[0] = UtilGetField( ModemGetValueRaw( Reply), 2, '"');
    } }
    
    if (UtilFileExists( TMP_FLAG_FAMILY_HYBRID_)) {
      if ((UtilStrCmp( UtilFileValue( FLASH_CFG_RAT), "3g") == 0) ||
           UtilFileExists( TMP_FLAG_LTE_3G_MODE)) {
         Cnti[0] = "EV-DO";
      } else {
        ModemQuery( "+CEREG=2");
        if (ModemQuery( "+CEREG?")) {
          for (int i = 0; Line = Reply[i]; i++) {
            if (UtilStrStr( Line, "+CEREG")) {
              Cnti[0] = UtilGetField( Line, 6, ',');
              switch (UtilStr2Int( Cnti[0])) {
                case 0: Cnti[0] = "GSM"  ; break;
                case 1: Cnti[0] = "GSM-C"; break;
                case 2: 
                case 3: 
                case 4: 
                case 5: 
                case 6: Cnti[0] = "EV-DO"; break;
                case 7: Cnti[0] = "LTE"  ; break;
  } } } } } } }

  //-------------------------------------
  // Web-UI modems
  //-------------------------------------

  const char * Path;
  long Port;
  char PostData[HTTP_POSTDATA_MAX_SIZE];
  int Method;
  if ( UtilFileExists( TMP_FLAG_FAMILY_HTTP_)) {
    // setup values based on the modem
    char ActiveModemIface[IFNAMSIZ];
    if (UtilStrStr( "fw770", ActiveType)) {
      DebugLogLn("Found fw770 modem");
      Path = HTTP_U770_PATH_RAT;
      Method = HTTP_U770_PATH_RAT_METHOD;
      Port = HTTP_U770_PORT;
      strncpy(PostData, HTTP_U770_POSTDATA_RAT, HTTP_POSTDATA_MAX_SIZE);
      UtilStrCpy(ActiveModemIface, UtilFileValue( TMP_VALUE_MODEM_NET), IFNAMSIZ);
      //const char * Cnti = UtilFileValue( TMP_VALUE_MODEM_CNTI);
    } else if (UtilStrStr( "pt295", ActiveType)) {
      DebugLogLn("Found pt295 modem");
      Path = HTTP_UML295_PATH_RAT;
      Method = HTTP_UML295_PATH_RAT_METHOD;
      Port = HTTP_UML295_PORT;
      UtilStrCpy(ActiveModemIface, UtilFileValue( TMP_VALUE_MODEM_NET), IFNAMSIZ);
    } else if (UtilStrStr( "k3773", ActiveType)) {
      DebugLogLn("Found k3773 modem");
      Path = HTTP_K3773_PATH_RAT;
      Method = HTTP_K3773_PATH_RAT_METHOD;
      Port = HTTP_K3773_PORT;
      UtilStrCpy(ActiveModemIface, UtilFileValue( TMP_VALUE_MODEM_NET), IFNAMSIZ);
    } else {
      ErrorLogLn( "Did not recognize modem: %s", ActiveModem);
      return;
    }

    // do the query
    CURL * Curl = curl_easy_init();
    CURLcode res;

    char HttpUiHost[40];
    char ModemGatewayFile[32];
    char Url[256];
    snprintf( ModemGatewayFile, 32, "/tmp/value.gateway_%s", ActiveModemIface);
    UtilStrCpy( HttpUiHost, UtilFileValue( ModemGatewayFile ), sizeof( HttpUiHost));
    snprintf( Url, 256, "http://%s/%s", HttpUiHost, Path);

    if (Curl) {
      curl_easy_setopt( Curl, CURLOPT_URL, Url);
      curl_easy_setopt( Curl, CURLOPT_PORT, Port);
      if (Method == HTTP_POST){
        curl_easy_setopt( Curl, CURLOPT_POST, 1);
        curl_easy_setopt( Curl, CURLOPT_POSTFIELDS, (void *) PostData);
      }
      curl_easy_setopt( Curl, CURLOPT_TIMEOUT, 2);
      struct DataBuf HttpBuf;
      // we're using a fixed size for more predictable behavior
      HttpBuf.size = 4096;
      HttpBuf.data = (char *) malloc(HttpBuf.size);
      curl_easy_setopt( Curl, CURLOPT_WRITEDATA, (void *)&HttpBuf);
      curl_easy_setopt( Curl, CURLOPT_WRITEFUNCTION, SaveHttpData);
      res = curl_easy_perform( Curl);
      curl_easy_cleanup( Curl);
      if (res == CURLE_OK) {
        // parse result
        int nb=1;
        int len;
        node_t *DataRoot = roxml_load_buf( HttpBuf.data);
        if (UtilStrStr( "fw770", ActiveType)){
          char XPath[XPATH_MAX_SIZE];
          strncpy(XPath, "/root/result", XPATH_MAX_SIZE);
          node_t **ResultTable = roxml_xpath(DataRoot, XPath, &nb);
          char *ResultContent = NULL;
          if (*ResultTable) {
            ResultContent = roxml_get_content( *ResultTable, NULL, 0, &len);
            roxml_release( ResultTable);
          }
          char *token = NULL;
          if(ResultContent) {
            token = strtok( ResultContent, "\n\r");
            //example ResultContent: 3gmodem\to\tx\t0\t0\tdown\t0\nwimax\to\tx\t1\t1\tdown\t0\nlte\to\tx\t0\t0\tdown\t0\n
            int i=0;
            const char *NetworkAvailabilityString;
            int NetworkAvailabilityField;
            if (UtilFileExists( "/tmp/flag.connected")){
              NetworkAvailabilityString = "up";
              NetworkAvailabilityField = 6;
            } else {
              NetworkAvailabilityString = "1";
              NetworkAvailabilityField = 4;
            }
            while(token != NULL){
              const char * EnabledField = UtilGetField( token, NetworkAvailabilityField, '\t');
              if (UtilStrStr(EnabledField, NetworkAvailabilityString))
                Cnti[0]=UtilGetField( token, 1, '\t');
              token = strtok( NULL, "\n\r");
              i++;
            }
            roxml_release( ResultContent);
            ResultContent = NULL;
          }
        }
        if (UtilStrStr( "pt295", ActiveType)){
          char XPath[XPATH_MAX_SIZE];
          strncpy(XPath, HTTP_UML295_PATH_RAT_XPATH, XPATH_MAX_SIZE);
          node_t **ResultTable = roxml_xpath(DataRoot, XPath, &nb);
          char *ResultContent = NULL;
          if (ResultTable) {
            ResultContent = roxml_get_content( *ResultTable, NULL, NULL, &len);
            roxml_release( ResultTable);
          }
          if (ResultContent) {
            if (UtilStrStr( "LTE", ResultContent)) Cnti[0] = "LTE";
            if (UtilStrStr( "CDMA", ResultContent)) Cnti[0] = "CDMA";
            if (UtilStrStr( "EVDO", ResultContent)) Cnti[0] = "EV-DO";
            if (UtilStrStr( "GSM", ResultContent)) Cnti[0] = "GSM";
            if (UtilStrStr( "1XRTT", ResultContent)) Cnti[0] = "1xRTT";
            roxml_release( ResultContent);
          }
        }
        if (UtilStrStr( "k3773", ActiveType)) {
          char XPath[XPATH_MAX_SIZE];
          strncpy(XPath, HTTP_K3773_PATH_RAT_XPATH, XPATH_MAX_SIZE);
          node_t **ResultTable = roxml_xpath(DataRoot, XPath, &nb);
          char *ResultContent = NULL;
          if (ResultTable) {
            ResultContent = roxml_get_content( *ResultTable, NULL, NULL, &len);
            roxml_release( ResultTable);
          }
          if (ResultContent) {
            switch (atoi(ResultContent)) {
			case 7: Cnti[0] = "HSDPA"; break;
            case 4: Cnti[0] = "UMTS"; break;
            case 3: Cnti[0] = "GSM"; break;
			}
            roxml_release( ResultContent);
          }
        }
        roxml_release( RELEASE_ALL);
        roxml_close( DataRoot);
      } else {
        ErrorLogLn( "Could not get resource at %s: %s", Url, curl_easy_strerror(res));
      }
      free(HttpBuf.data);
    }
  }
  if (Cnti[0] && CntiN == 0)
    CntiN = 1;

  for (int i = 0; i < CntiN; i++) {
    if (UtilStrStr( "CDMA 1X"                , Cnti[i])) Cnti[i] = "CDMA-1X";
    if (UtilStrStr( "CDMA"                   , Cnti[i])) Cnti[i] = "CDMA-1X";
    if (UtilStrStr( "No System or Analog"    , Cnti[i])) Cnti[i] = "1xRTT";
    if (UtilStrStr( "3gmodem"                , Cnti[i])) Cnti[i] = "EV-DO";
    if (UtilStrStr( "EV-DO Rev A,EV-DO Rev 0", Cnti[i])) Cnti[i] = "EV-DO";
    if (UtilStrStr( "HDR"                    , Cnti[i])) Cnti[i] = "EV-DO";
    if (UtilStrStr( "GSM+EGPRS,GSM-C"        , Cnti[i])) Cnti[i] = "GSM"  ;
    if (UtilStrStr( "lte"                    , Cnti[i])) Cnti[i] = "LTE"  ;
    if (UtilStrStr( "wimax"                  , Cnti[i])) Cnti[i] = "WiMAX"  ;
  }

  for (int i = 0; i < CntiN; i++)
    if (UtilStrStr( "IS-95,CDMA-1X,GSM,GPRS,EDGE,1xRTT", Cnti[i])) Flag3g="2g";
  
  for (int i = 0; i < CntiN; i++)
    if (UtilStrStr( "EV-DO,UMTS,WCDMA,HSUPA,HSDPA,"
				"CDMA-1XEVDO,"
				"UMTS,"
                "HSDPA/HSUPA,HSPA,UTRAN,"
                "UTRAN+HSDPA,UTRAN+HSUPA,"
                "UTRAN+HSDPA/HSUPA,E-UTRAN", Cnti[i])) Flag3g = "3g";
              
  for (int i = 0; i < CntiN; i++)
    if (UtilStrStr( "WiMAX,LTE,HSPA+", Cnti[i])) Flag3g = "4g";
  
  if (UtilStrStr( "-", Flag3g)) {
    Cnti[0] = "---";
    UtilFlagSet( TMP_FLAG_WIRELESS_UNKNOWN);
    UtilFileDelete( TMP_FLAG_WIRELESS_2G);
    UtilFileDelete( TMP_FLAG_WIRELESS_3G);
    UtilFileDelete( TMP_FLAG_WIRELESS_4G);
  }
  if (UtilStrStr( "2g", Flag3g)) {
    UtilFileDelete( TMP_FLAG_WIRELESS_UNKNOWN);
    UtilFlagSet( TMP_FLAG_WIRELESS_2G);
    UtilFileDelete( TMP_FLAG_WIRELESS_3G);
    UtilFileDelete( TMP_FLAG_WIRELESS_4G);
  }
  if (UtilStrStr( "3g", Flag3g)) {
    UtilFileDelete( TMP_FLAG_WIRELESS_UNKNOWN);
    UtilFileDelete( TMP_FLAG_WIRELESS_2G);
    UtilFlagSet( TMP_FLAG_WIRELESS_3G);
    UtilFileDelete( TMP_FLAG_WIRELESS_4G);
  }
  if (UtilStrStr( "4g", Flag3g)) {
    UtilFileDelete( TMP_FLAG_WIRELESS_UNKNOWN);
    UtilFileDelete( TMP_FLAG_WIRELESS_2G);
    UtilFileDelete( TMP_FLAG_WIRELESS_3G);
    UtilFlagSet( TMP_FLAG_WIRELESS_4G);
  }

  UtilFileSet( TMP_VALUE_MODEM_CNTI, Cnti[0]);

  ModemQueryFree( Reply);
}

//--------------------------------------------------------------------------
// Query the signal strength and quality metrics of the active modem.  This
// routine is a functional replacement for the old rssi.sh script.
//--------------------------------------------------------------------------

static void QueryRssi( void) {
  Reply_t Reply = NULL;
  int Pct = 0, Dbm = -114, Rssi = -1, Csq = -1, i;
  ccptr rc = "err", Cid = "", Lac = "", Sid = "", Nid = "", Band = "", Line = "", Quality = "";
  float Ecio = 0.0, Sinr = 0.0, Snr = -99.0;
  float _Rsrp = -199.0, _Rsrq = 0.0;
  ccptr Rsrp = "", Rsrq = "", bars = NULL;
  ccptr bars_str[] = { "0", "1", "2", "3", "4", "5", "" };
  
  if (UtilStrCmp( "USR5637", ActiveType) == 0) {
  
    rc = "5";
    
  } else if(UtilFileExists( TMP_FLAG_FAMILY_QMI_)) {
    //-------------------------------------
    // QMI Family modems
    //-------------------------------------
    if (ModemQmiQuery( "--nas-get-serving-system")) {
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "SID:")) {
          Line = Reply[i++];
          DebugLogLn(Line);
          Sid = UtilGetField( Line, 2, '\'');
          Sid = UtilUcase( Sid);
        }
        if (UtilStrStr( Line, "NID:")) {
          Line = Reply[i++];
          DebugLogLn(Line);
          Nid = UtilGetField( Line, 2, '\'');
          Nid = UtilUcase( Nid);
        }
        if (UtilStrStr( Line, "cell ID:")) {
          DebugLogLn(Line);
          Cid = UtilGetField( Line, 2, '\'');
        }
        if (UtilStrStr( Line, "location area code:")) {
          DebugLogLn(Line);
          Lac = UtilGetField( Line, 2, '\'');
    } } }

    if (ModemQmiQuery( "--nas-get-signal-strength")) {
      //TODO: this isn't very accurate; use libqmi instead.

      /* To prevent skipping over needed values, perform an individual scan
         of the output for each desired value */
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "Current:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          Dbm = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          Rssi = Dbm; /* leave Rssi untouched */
          /* some modems (K4511)  report dbm outside our range, pull them in */
          if (-51 < Dbm && Dbm <= -10)
            Dbm = -51;
          UtilRangeCheck( -113, -51, -115, Dbm);
          Csq = (Dbm + 113) / 2;
          break;
        } else if (UtilStrStr( Line, "RSSI:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          Dbm = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          Rssi = Dbm; /* leave Rssi untouched */
          /* some modems (K4511)  report dbm outside our range, pull them in */
          if (-51 < Dbm && Dbm <= -10)
            Dbm = -51;
          UtilRangeCheck( -113, -51, -115, Dbm);
          Csq = (Dbm + 113) / 2;
          break;
      } }
      DebugLogLn("DBM: %d", Dbm);
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "ECIO:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          Ecio = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          break;
      } }
      DebugLogLn("ECIO: %f", Ecio);
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "RSRP:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          _Rsrp = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          Rsrp = UtilFloat2Str(_Rsrp);
          break;
      } }
      DebugLogLn("RSRP: %f", _Rsrp);
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "SNR:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          Snr = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          break;
      } }
      DebugLogLn("SNR: %f", Snr);
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "RSRQ:")) {
          Line = Reply[++i];
          DebugLogLn(Line);
          _Rsrq = UtilStr2Float(UtilGetField(UtilGetField( Line, 4, '\''), 1, ' '));
          Rsrq = UtilFloat2Str(_Rsrq);
          break;
      } }
      DebugLogLn("RSRQ: %f", _Rsrq);
      DebugLogLn("=============");
      for (int i = 0; Line = Reply[i]; i++) {
        if (UtilStrStr( Line, "SINR:")) {
          DebugLogLn(Line);
          Sinr = UtilStr2Float(UtilGetField(UtilGetField( Line, 2, '\''), 1, ' '));
          break;
      } } 
      DebugLogLn("SINR: %f", Sinr);
    } } else {
  
    //-------------------------------------
    // CDMA Family 1 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA1_)) {
      if (ModemQuery( "!RSSI?")) Dbm = UtilStr2Int( ModemGetValueRaw( Reply));
      if (ModemQuery( "!ECIO?")) Ecio = UtilStr2Float( ModemGetValue( Reply, "Ec/Io"));
      if (ModemQuery( "+CSS"  )) {
        Band = ModemRemapCssBand( Reply);
        Sid  = ModemRemapCssSid ( Reply);
        Nid  = ModemRemapCssNid ( Reply);
      }
      Rssi = Dbm; /* leave Rssi untouched */
      UtilRangeCheck( -113, -51, -115, Dbm);
      Csq = (Dbm + 113) / 2;
      if ((*Sid == 0) || (*Nid == 0)) {
        if (ModemQuery( "~NAMVAL?0")) {
          if (*Sid == 0) Sid = ModemGetValue( Reply, "SID");
          if (*Nid == 0) Nid = ModemGetValue( Reply, "NID");
    } } }
    
    //-------------------------------------
    // CDMA Family 2 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA2_)) {
      if (ModemQuery( "$NWRSSI")) {
        Line = ModemGetValueRaw( Reply);
        Dbm = -1 * abs( UtilStr2Int( UtilGetField( Line, 2, '=')));
        Rssi = Dbm; /* leave Rssi untouched */
        /* The 551L sometimes reports dbm outside our range; pull them in */
        if (Dbm > -51 && Dbm < 0) Dbm = -51;
        UtilRangeCheck( -113, -51, -115, Dbm);
        Csq = (Dbm + 113) / 2;
        if (Dbm == -115) {
          UtilFileSet( FLASH_VALUE_RSSI_NWRSSI_ERR, Line);
        } else {
          UtilFileSet( FLASH_VALUE_RSSI_NWRSSI_OK , Line);
      } }
      if (ModemQuery( "+CSS")) {
        Line = ModemGetValueRaw( Reply);
        switch (UtilStr2Int( Line)) {
          case 1: Band = " 800"; break;
          case 2: Band = "1900"; break;
        }
        Sid = UtilGetField( Line, 3, ',');
        Nid = UtilGetField( Line, 4, ',');
    } }

    //-------------------------------------
    // CDMA Family 3 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_CDMA3_)) {
      if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( UtilGetField( ModemGetValueRaw( Reply), 1, ','));
      UtilRangeCheck( 0, 31, -1, Csq);
      Dbm = (2 * Csq) - 113;
      Rssi = Dbm; /* leave Rssi untouched */
      if (ModemQuery( "$MSID")) Sid = UtilGetField( ModemGetValueRaw( Reply), 2, ' ');
    }
    
    //-------------------------------------
    // GSM Family 1 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_GSM1_)) {
      if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( ModemGetValueIndex( Reply, "+CSQ", 1));
      UtilRangeCheck( 0, 31, -1, Csq);
      Dbm = (2 * Csq) - 113;
      Rssi = Dbm; /* leave Rssi untouched */
      if (ModemQuery( "+ECIO?")) Ecio = UtilStr2Float( ModemGetValueRaw( Reply));
      if (ModemQuery( "+CREG=2")) {
        if (ModemQuery( "+CREG?")) {
          Line = ModemGetValue( Reply, "+CREG");
          Cid = UtilGetField( Line, 4, ',');
          Lac = UtilGetField( Line, 3, ',');
      } }
      if (ModemQuery( "!GSTATUS?")) {
        for (int i = 0; Line = Reply[i]; i++) {
          if (UtilStrStr( Line, "band")) {
            // The 313U network band is listed with a prefix on all but LTE connections.
            if ( UtilStrStr( "sw313", ActiveType)) {
              if ( UtilStrStr( "LTE", UtilFileValue( TMP_VALUE_MODEM_CNTI))) {
                Band = UtilGetField( Line, 3, ' ');
              } else {
                Band = UtilGetField( Line, 4, ' ');
            } } else {
              Band = UtilGetField( UtilGetField( Line, 3, ' '), 2, 'A');
            }
          } else if (UtilStrStr( Line, "RSRP")) {
            Rsrp = UtilGetField( Line, 3, ' ');
          } else if (UtilStrStr( Line, "RSRQ")) {
            Rsrq = UtilGetField( Line, 3, ' ');
          } else if (UtilStrStr( Line, "SINR")) {
            Sinr = UtilStr2Float(UtilGetField( Line, 3, ' '));
      } } }
      if (*Band == 0) {
        switch (UtilStr2Int( UtilGetField( Line, 2, ' '))) {
          case 1: Band =  "800"; break;
          case 2: Band = "1900"; break;
    } } }

    //-------------------------------------
    // GSM Family 2 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_GSM2_)) {
      if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( ModemGetValueIndex( Reply, "+CSQ", 1));
      UtilRangeCheck( 0, 31, -1, Csq);
      Dbm = (2 * Csq) - 113;
      Rssi = Dbm; /* leave Rssi untouched */
      if (ModemQuery( "+CREG=2")) {
        if (ModemQuery( "+CREG?")) {
          Line = ModemGetValue( Reply, "+CREG");
          Cid = UtilGetField( Line, 4, ',');
          Lac = UtilGetField( Line, 3, ',');
    } } }
    
    //-------------------------------------
    // GSM Family 3 modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_GSM3_)) {
      if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( ModemGetValueIndex( Reply, "+CSQ", 1));
      UtilRangeCheck( 0, 31, -1, Csq);
      Dbm = (2 * Csq) - 113;
      if (UtilStrStr( "ZTE", UtilFileValue( TMP_VALUE_MODEM_MAKER))) {
        if (ModemQuery( "+ZRSSI")) {
          Line = ModemGetValueRaw( Reply);
          Dbm = UtilStr2Int( UtilGetField( Line, 1, ','));
          Ecio = UtilStr2Float( UtilGetField( Line, 2, ','));
          Ecio = -Ecio / 2.0;
      } }
      Rssi = Dbm; /* leave Rssi untouched */
      if (ModemQuery( "+CREG=2")) {
        if (ModemQuery( "+CREG?")) {
          Line = ModemGetValue( Reply, "+CREG");
          Cid = UtilGetField( Line, 4, ',');
          Lac = UtilGetField( Line, 3, ',');
    } } }
    
    //-------------------------------------
    // HYBRID modems
    //-------------------------------------
    
    if (UtilFileExists( TMP_FLAG_FAMILY_HYBRID_)) {
      if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( ModemGetValueIndex( Reply, "+CSQ", 1));
      if (Csq == -1) {
        if (ModemQuery( "+CSQ")) Csq = UtilStr2Int( ModemGetValueIndex( Reply, "1:[", 1));
      }
      UtilRangeCheck( 0, 31, -1, Csq);
      Dbm = (2 * Csq) - 113;
      Rssi = Dbm; /* leave Rssi untouched */
      if (UtilStrStr( "pt290", ActiveType)) {
        if (ModemQuery( "+CEREG=2")) {
          if (ModemQuery( "+CEREG?")) {
            Line = ModemGetValue( Reply, "+CEREG");
            Cid = UtilGetField( Line, 5, ',');
            Lac = UtilGetField( Line, 3, ',');
    } } } }
    
    //-------------------------------------
    // Done querying real modems
    //-------------------------------------
    
    ModemQueryFree( Reply);

    //-------------------------------------
    // Web-UI modems
    //-------------------------------------

    const char * Path;
    long Port;
    char PostData[HTTP_POSTDATA_MAX_SIZE];
    int Method;
    const char * ActiveModemIface;
    char Cnti[CNTI_MAX_SIZE];
    UtilStrCpy( Cnti, UtilFileValue( TMP_VALUE_MODEM_CNTI), sizeof( Cnti));
    if ( UtilFileExists( TMP_FLAG_FAMILY_HTTP_)) {
      // setup values based on the modem
      if (UtilStrStr( "fw770", ActiveType)) {
        Path = HTTP_U770_PATH_RSSI;
        Method = HTTP_U770_PATH_RSSI_METHOD;
        Port = HTTP_U770_PORT;
        if (UtilStrStr(Cnti, "EV-DO"))
          strncpy(PostData, HTTP_U770_POSTDATA_3G_RSSI, HTTP_POSTDATA_MAX_SIZE);
        if (UtilStrStr(Cnti, "WiMAX"))
          strncpy(PostData, HTTP_U770_POSTDATA_WIMAX_RSSI, HTTP_POSTDATA_MAX_SIZE);
        if (UtilStrStr(Cnti, "LTE"))
          strncpy(PostData, HTTP_U770_POSTDATA_LTE_RSSI, HTTP_POSTDATA_MAX_SIZE);
        ActiveModemIface = UtilFileValue( TMP_VALUE_MODEM_NET);
      } else if (UtilStrStr( "pt295", ActiveType)) {
        Path = HTTP_UML295_PATH_RSSI;
        Method = HTTP_UML295_PATH_RSSI_METHOD;
        Port = HTTP_UML295_PORT;
        ActiveModemIface = UtilFileValue( TMP_VALUE_MODEM_NET);
      } else if (UtilStrStr( "k3773", ActiveType)) {
        Path = HTTP_K3773_PATH_RSSI;
        Method = HTTP_K3773_PATH_RSSI_METHOD;
        Port = HTTP_K3773_PORT;
        ActiveModemIface = UtilFileValue( TMP_VALUE_MODEM_NET);
      } else {
        ErrorLogLn( "Did not recognize modem: %s", ActiveModem);
        return;
      }

      // do the query
      CURL * Curl = curl_easy_init();
      CURLcode res;

      char HttpUiHost[40];
      char ModemGatewayFile[32+IFNAMSIZ];
      char Url[256];
      snprintf( ModemGatewayFile, 32+IFNAMSIZ, "/tmp/value.gateway_%s", ActiveModemIface);
      UtilStrCpy( HttpUiHost, UtilFileValue( ModemGatewayFile ), sizeof( HttpUiHost));
      snprintf( Url, 256, "http://%s/%s", HttpUiHost, Path);

      if (Curl) {
        curl_easy_setopt( Curl, CURLOPT_URL, Url);
        curl_easy_setopt( Curl, CURLOPT_PORT, Port);
        if (Method == HTTP_POST){
          curl_easy_setopt( Curl, CURLOPT_POST, 1);
          curl_easy_setopt( Curl, CURLOPT_POSTFIELDS, (void *) PostData);
        }
        curl_easy_setopt( Curl, CURLOPT_TIMEOUT, 2);
        struct DataBuf HttpBuf;
        // we're using a fixed size for more predictable behavior
        HttpBuf.size = 8192;
        HttpBuf.data = (char *) malloc(HttpBuf.size);
        curl_easy_setopt( Curl, CURLOPT_WRITEDATA, (void *)&HttpBuf);
        curl_easy_setopt( Curl, CURLOPT_WRITEFUNCTION, SaveHttpData);
        res = curl_easy_perform( Curl);
        curl_easy_cleanup( Curl);
        if (res == CURLE_OK) {
          // parse result
          node_t *DataRoot = roxml_load_buf( HttpBuf.data);
          node_t *Result;
          node_t *ResultText;
          char *ResultContent = NULL;
          char *token;
          node_t **ResultTable;
          int nb = 1;
          int len;
          if (UtilStrStr( "fw770", ActiveType)){
            if (UtilStrStr( Cnti, "WiMAX") || UtilStrStr( Cnti, "LTE")){
              char XPath[XPATH_MAX_SIZE];
              strncpy(XPath, HTTP_U770_PATH_RSSI_XPATH_4G, XPATH_MAX_SIZE);
              ResultTable = roxml_xpath(DataRoot, XPath, &nb);
              if (ResultTable) {
                ResultContent = roxml_get_content( *ResultTable, NULL, 0, &len);
                roxml_release( ResultTable);
                ResultTable = NULL;
              }
              if (ResultContent) {
                token = strtok( ResultContent, ", ");
                int i=0;
                while(token != NULL){
                  switch(i){
                    case 0:
                      Dbm = atoi( token);
                      Rssi = Dbm; /* leave Rssi untouched */
                      Csq = (Dbm + 113) / 2;
                      break;
                    case 1:
                      Ecio = atof( token);
                      break;
                  }
                  token = strtok( NULL, ", ");
                  i++;
                }
                roxml_release(ResultContent);
                ResultContent = NULL;
              }
            } else if (UtilStrStr( Cnti, "EV-DO")){
              //example Http.date for a 3gmodem connection:
              // <root><result>-89 dBm</result><result>-5.0 dB</result><result>Disconnected</result><result>eHRPD</result><result>accelecon44@sprintpcs.com</result></root>

              //Dbm is in result 0
              char XPath[XPATH_MAX_SIZE];
              strncpy(XPath, HTTP_U770_PATH_RSSI_XPATH_3G, XPATH_MAX_SIZE);
              ResultTable = roxml_xpath(DataRoot, XPath, &nb);
              if (ResultTable){
                ResultContent = roxml_get_content( *ResultTable, NULL, 0, &len);
                roxml_release( ResultTable);
                ResultTable = NULL;
              }
              if (ResultContent){
                token = strtok( ResultContent, " ");
                Dbm = atoi( token);
                Rssi = Dbm; /* leave Rssi untouched */
                Csq = (Dbm + 113) / 2;
                roxml_release( ResultContent);
                ResultContent = NULL;
              }
              //Ecio is in result 1
              strncpy(XPath, HTTP_U770_PATH_ECIO_XPATH_3G, XPATH_MAX_SIZE);
              ResultTable = roxml_xpath(DataRoot, XPath, &nb);
              if (ResultTable) {
                ResultContent = roxml_get_content( *ResultTable, NULL, 0, &len);
                roxml_release( ResultTable);
                ResultTable = NULL;
              }
              if (ResultContent){
                token = strtok( ResultContent, " ");
                Ecio = atof( token);
                roxml_release( ResultContent);
                ResultContent = NULL;
              }
            }
          }
          if (UtilStrStr( "pt295", ActiveType)){
            char XPath[XPATH_MAX_SIZE];
            //RSSI
            strncpy(XPath, HTTP_UML295_PATH_RSSI_XPATH, XPATH_MAX_SIZE);
            node_t **ResultTable = roxml_xpath(DataRoot, XPath, &nb);
            if (ResultTable) {
              ResultContent = roxml_get_content( *ResultTable, NULL, NULL, &len);
              roxml_release( ResultTable);
              ResultTable = NULL;
            }
            if (ResultContent){
              Dbm = atoi(ResultContent);
              Rssi = Dbm; /* leave Rssi untouched */
              Csq = (Dbm + 113) / 2;
              roxml_release( ResultContent);
              ResultContent = NULL;
            }
            //ECIO
            strncpy(XPath, HTTP_UML295_PATH_ECIO_XPATH, XPATH_MAX_SIZE);
            ResultTable = roxml_xpath(DataRoot, XPath, &nb);
            if (ResultTable) {
              ResultContent = roxml_get_content( *ResultTable, NULL, NULL, &len);
              roxml_release( ResultTable);
              ResultTable = NULL;
            }
            if (ResultContent){
              Ecio = atof(ResultContent);
              roxml_release( ResultContent);
              ResultContent = NULL;
            }
          }
          if (UtilStrStr( "k3773", ActiveType)) {
            char XPath[XPATH_MAX_SIZE];
            //RSSI
            strncpy(XPath, HTTP_K3773_PATH_RSSI_XPATH, XPATH_MAX_SIZE);
            node_t **ResultTable = roxml_xpath(DataRoot, XPath, &nb);
            if (ResultTable) {
              ResultContent = roxml_get_content( *ResultTable, NULL, NULL, &len);
              roxml_release( ResultTable);
              ResultTable = NULL;
            }
            if (ResultContent){
              Dbm = (((atoi(ResultContent) * 62) / 100) - 113);
              Rssi = Dbm; /* leave Rssi untouched */
              Csq = (Dbm + 113) / 2;
              roxml_release( ResultContent);
              ResultContent = NULL;
            }
          }
          roxml_release ( RELEASE_ALL);
          roxml_close( DataRoot);
        } else {
          ErrorLogLn( "Could not get resource at %s: %s", Url, curl_easy_strerror(res));
        }
        free(HttpBuf.data);
      }
    }
  }

  // we need to play fixup for the NetGear 340 and 341 as they reports their own
  // bars and handle signal strength differently depending on the network type.
  if ( UtilStrStr("sw340", ActiveType) || UtilStrStr("sw341", ActiveType) ) {
    if (UtilFileExists(TMP_FLAG_WIRELESS_4G)) {
      int rsrp = (int) _Rsrp, rsrp_bars = 5, snr_bars = 5;
      // for 4G (LTE) use RSRP for our signal strength, if we have SNR values
      // then we use that too,  otherwise just use RSRP
      // Calculate bars the AT&T way, first RSRP
      if      (rsrp > -85 ) rsrp_bars = 5;
      else if (rsrp > -95 ) rsrp_bars = 4;
      else if (rsrp > -105) rsrp_bars = 3;
      else if (rsrp > -115) rsrp_bars = 2;
      else                  rsrp_bars = UtilFileExists( TMP_FLAG_CONNECTED)?1:0;
      // Now use SNR
      if      (Snr >= 13 ) snr_bars = 5;
      else if (Snr >= 4.5) snr_bars = 4;
      else if (Snr >= 1  ) snr_bars = 3;
      else if (Snr > -3  ) snr_bars = 2;
      else if (Snr > -99 ) snr_bars = UtilFileExists( TMP_FLAG_CONNECTED)?1:0;

      if (rsrp_bars < snr_bars)
        bars = bars_str[rsrp_bars];
      else
        bars = bars_str[snr_bars];
      // Scale rsrp to be more like an RSSI value everyone is looking for.
      // consensus seems to be Rsrp reads 20db lower than equivalent RSSI
      Dbm = rsrp + 20;
      if (Dbm < -113 && Dbm >= -115) Dbm = -113;
      if (Dbm > -51 && Dbm < 0) Dbm = -51;
    } else if (UtilFileExists(TMP_FLAG_WIRELESS_3G)) {
      // we are supposed to use RSCP here but I see no way to get it
      // for the sw340 (at+rscp reports not-implemented)
      if      (Dbm > -80)  bars = "5";
      else if (Dbm > -90)  bars = "4";
      else if (Dbm > -100) bars = "3";
      else if (Dbm > -106) bars = "2";
      else                 bars = UtilFileExists(TMP_FLAG_CONNECTED)?"1":"0";
    } else {
      if      (Dbm > -80)  bars = "5";
      else if (Dbm > -89)  bars = "4";
      else if (Dbm > -98)  bars = "3";
      else if (Dbm > -104) bars = "2";
      else                 bars = UtilFileExists(TMP_FLAG_CONNECTED)?"1":"0";
    }
    // make the value more suitable for us to display
    if (Dbm < -113 && Dbm >= -115) Dbm = -113;
    if (Dbm > -51 && Dbm < 0) Dbm = -51;
  }
    
  // If dBm is in the valid range -113 (0%) to -51 (100%) then
  // calculate the percentage signal strength, else leave it at
  // the default value of 0%.

  if ((-113 <= Dbm) && (Dbm <= -51)) {
    Pct = ((Dbm + 113) * 100) / 62;
  }
  
  // Export the values we have pulled from the modem.
  
  UtilFileSet( TMP_VALUE_RSSI_PCT, UtilInt2Str( Pct));
  UtilFileSet( TMP_VALUE_RSSI_DBM, UtilInt2Str( Dbm));
  UtilFileSet( TMP_VALUE_RSSI_RSSI,UtilInt2Str( Rssi));
  UtilFileSet( TMP_VALUE_RSSI_CSQ, UtilInt2Str( Csq));
  
  UtilFileSet( TMP_VALUE_RSSI_ECIO, UtilFloat2Str( Ecio));
  UtilFileSet( TMP_VALUE_RSSI_SINR, UtilFloat2Str( Sinr));
  UtilFileSet( TMP_VALUE_RSSI_SNR, UtilFloat2Str( Snr));
  
  // convert Cid/Lac hex to decimal. QMI values are alread in decimal.
  if ( !UtilFileExists( TMP_FLAG_FAMILY_QMI_)) {
    char Buffer[32];
    if (Cid) {
      sprintf( Buffer, "%u", UtilHexToBit32( Cid));
      Cid = TempDup( Buffer);
    }
    if (Lac) {
      sprintf( Buffer, "%u", UtilHexToBit32( Lac));
      Lac = TempDup( Buffer);
    }
  }
  bool CidOrLac = UtilFileSet( TMP_VALUE_RSSI_CID , Cid ) ||
    UtilFileSet( TMP_VALUE_RSSI_LAC , Lac );

  bool SidOrNid = UtilFileSet( TMP_VALUE_RSSI_SID , Sid ) ||
    UtilFileSet( TMP_VALUE_RSSI_NID , Nid );

  UtilFileSet( TMP_VALUE_RSSI_BAND, Band);

  UtilFileSet( TMP_VALUE_RSSI_RSRP, Rsrp);
  UtilFileSet( TMP_VALUE_RSSI_RSRQ, Rsrq);
  
  // Determine quality and signal bars, and output to the proper
  // value and flag files.
  // Changes here need to be reflected in qmodem.cpp, rssi.sh and nbfx_notifier.sh

  if (Dbm < -109) { Quality = "Bad";      } else
  if (Dbm < -93)  { Quality = "Marginal"; } else 
  if (Dbm < -83)  { Quality = "OK";       } else
  if (Dbm < -73)  { Quality = "Good";     } else 
  {                 Quality = "Excellent";     }

  
  if (bars)        { rc = bars;} else
  if (Dbm >= -75)  { rc = "5"; } else 
  if (Dbm >= -83)  { rc = "4"; } else 
  if (Dbm >= -95)  { rc = "3"; } else 
  if (Dbm >= -105) { rc = "2"; } else 
  if (Dbm >= -110) { rc = "1"; } else 
  {                  rc = UtilFileExists( TMP_FLAG_CONNECTED) ? "1" : "0"; }
  
  if (fptr f = fopen( TMP_VALUE_SIGNAL, "w")) {
    fprintf( f, "%s (%d dBm) (%d%%)", Quality, Dbm, Pct);
    fclose( f);
  }
  
  if (UtilStrStr( "err", rc)) {
    UtilFileDelete( TMP_VALUE_RSSI_SIG);
    UtilFileDelete( TMP_FLAG_RSSI_UPDATE);
    UtilFlagForce( TMP_FLAG_RSSI_ERR);
  } else {
    UtilFileDelete( TMP_FLAG_RSSI_ERR);
    if (UtilFileSet( TMP_VALUE_RSSI_SIG, rc))
      UtilFlagForce( TMP_FLAG_RSSI_UPDATE);
  }

  // Seatbelts
  
  if (SidOrNid) UtilFlagForce( TMP_FLAG_SIDNID_UPDATE);
  if (CidOrLac) UtilFlagForce( TMP_FLAG_CIDLAC_UPDATE);
  
  UtilReplyFree( Reply);
}

//--------------------------------------------------------------------------
// Update the global modem identifier, command port and usb vid/pid values,
// then sample the active modem signal strength and network connection type
// and update the appropriate flag/value files.
//--------------------------------------------------------------------------

static void RefreshQmodemRssi3g( void) {
  RefreshActiveModem();
  if (UtilFileExists( TMP_FLAG_MODEM_GOOD)) {
    if (UtilFileExists( TMP_FLAG_DUAL_PORT) || UtilStrStr( "nt551,nt679", ActiveType) || UtilFileExists( TMP_FLAG_FAMILY_QMI_) || UtilFileExists( TMP_FLAG_FAMILY_HTTP_)) {
      Query3g  ();
      QueryRssi();
    } else {
      Qmodem();
} } }

void sigterm_handler(int sig) {
  DebugLogLn( ">>>>>>>>>> SIGTERM received. Exiting cleanly...");
  clean_exit = 1;
}

//--------------------------------------------------------------------------
// Sample the modem signal strength and network connection type every 2
// seconds. Save the results into various files found in /var/run/modems/*modem_name*/
//--------------------------------------------------------------------------

int main( int argc, cptr argv[]) {
  void sigterm_handler(int sig);
  struct sigaction sa;
  sa.sa_handler = sigterm_handler;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  if (sigaction(SIGTERM, &sa, NULL) == -1) {
        perror("sigaction");
  }
  openlog(NULL, LOG_CONS, LOG_LOCAL1);
  curl_global_init( CURL_GLOBAL_NOTHING);
  DebugLogLn( "Start");
  while (true) {
    if (clean_exit == 1) {
      break;
    } else {
      RefreshQmodemRssi3g();
    }
    UtilSleep( 8);
  }
  //It will never get here, but...
  closelog();
}
//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
