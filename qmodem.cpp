//--------------------------------------------------------------------------
// Module: USB Modem Binary Query                            +------------+
// Author: Michael Nagy                                      | qmodem.cpp |
// Date:   15-Aug-2012                                       +------------+
//--------------------------------------------------------------------------

// Serial port defaults to /dev/ttyUSB1 except for Franklin modems, where
// it defaults to /dev/ttyUSB2.  It can also be explicitly specified on
// the command line, as can vid and pid (which are otherwise taken from
// the value files in the /tmp/ directory).

#include "main.h"

static struct usb_device     * UsbDevice    = NULL;
static struct usb_dev_handle * UsbDevHandle = NULL;

// If we get a valid csq value, set SigRc to something like '3' and
// update the various /var/run/modems/<modem>/rssi_* files in the same way that the
// /nb/rssi.sh script would have done.

static ccptr SigRc = "err";

static uint16_t ValueExportCsq = 0;
static uint16_t ValueExportDbm = 0;
static uint16_t ValueExportPct = 0;

// Do the actual export.

static void ExportCsqDbmSignal( void) {
  ccptr Quality;

  // Changes here need to be reflected in main.cpp, rssi.sh and nbfx_notifier.sh
  // note the logic here is a little different as we are using implied
  // negative numbers :-(

  if (ValueExportDbm > 109) { Quality = "Bad";      } else
  if (ValueExportDbm > 93)  { Quality = "Marginal"; } else 
  if (ValueExportDbm > 83)  { Quality = "OK";       } else
  if (ValueExportDbm > 73)  { Quality = "Good";     } else 
  {                           Quality = "Excellent";}

  
  if (ValueExportDbm <= 75)  { SigRc = "5"; } else 
  if (ValueExportDbm <= 83)  { SigRc = "4"; } else 
  if (ValueExportDbm <= 95)  { SigRc = "3"; } else 
  if (ValueExportDbm <= 105) { SigRc = "2"; } else 
  if (ValueExportDbm <= 110) { SigRc = "1"; } else 
  {                            SigRc = UtilFileExists( TMP_FLAG_CONNECTED) ?
                                       "1" : "0"; }
  
  if (ValueExportDbm >= 113)
    ValueExportPct = 0;
  else if (ValueExportDbm <= 51)
    ValueExportPct = 100;
  else
    ValueExportPct = ((113-ValueExportDbm)*100)/62;

  static char Signal[80];
  sprintf( Signal, "%s (-%d dBm) (%d%%)", Quality, ValueExportDbm, ValueExportPct);
  static char ExportValue[128];
  sprintf( ExportValue, "%d", ValueExportCsq);
  UtilFileSet( TMP_VALUE_RSSI_CSQ, ExportValue);
  sprintf( ExportValue, "-%d", ValueExportDbm);
  UtilFileSet( TMP_VALUE_RSSI_DBM, ExportValue);
  sprintf( ExportValue, "-%d", ValueExportPct);
  UtilFileSet( TMP_VALUE_RSSI_PCT, ExportValue);
  UtilFileSet( TMP_VALUE_SIGNAL, Signal);
  QmodemLogLn( "Export Signal '%s'", Signal);
}

// Changes here need to be reflected in main.cpp, rssi.sh and nbfx_notifier.sh

static bool CommitCsqDbm( uint16_t Csq, uint16_t Dbm) {
  ValueExportCsq = Csq;
  ValueExportDbm = Dbm;
  return true;
}

// Valid dbm values are 51..113, with an implied negative sign.

static bool ExportDbm( uint16_t Dbm) {
  if ((Dbm >= 51) && (Dbm <= 113)) {
    uint16_t Csq = (113 - Dbm) / 2;
    return CommitCsqDbm( Csq, Dbm);
  }
  return false;
}

// Valid csq values are 0..31.

static bool ExportCsq( uint16_t Csq) {
  if (Csq <= 31) {
    uint16_t Dbm = 113 - (2 * Csq);
    return CommitCsqDbm( Csq, Dbm);
  }
  return false;
}

// Sierra cns reports rssi in -1.0 dbm units.

static bool SwExportRssi( uint16_t Rssi) {
  return ExportDbm( Rssi);
}

// Sierra cns reports active as 1 for an activated modem.

static bool SwExportActive( uint16_t Active) {
  QmodemLogLn( "SwExportActive(%d)", Active);
  return false;
}

// Sierra cns reports rssi in -1.0 dbm units, ecio in -0.5 dbm units

static bool SwExportEcio( uint16_t RssiCdma, uint16_t EcioCdma, uint16_t RssiEvdo, uint16_t EcioEvdo) {
  QmodemLogLn( "SwExportEcio(%d,%d,%d,%d)", RssiCdma, EcioCdma, RssiEvdo, EcioEvdo);
  return false;
}

//----------------------------------------------------------------------------
// Serial i/o support for usb ports.
//----------------------------------------------------------------------------

static bool UsbSerialClose( int & PortHandle) {
  if (PortHandle > 0) {
    UtilSleep_mS( 20);
    close( PortHandle);
    PortHandle = 0;
    return true;
  }
  PortHandle = 0;
  return false;
}

static bool UsbSerialOpen( int & PortHandle, ccptr SerialDevice) {
  if (PortHandle) {
    ErrorLogLn( "UsbSerialOpen('%s') bad handle %d", SerialDevice, PortHandle);
    return false;
  }
  PortHandle = open( SerialDevice, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (PortHandle <= 0) {
    PortHandle = 0;
    ErrorLogLn( "UsbSerialOpen('%s') open %d", SerialDevice, PortHandle);
    return false;
  }
  int BaudRateCode = B115200;
  struct termios DeviceAttributes;
  int rc = tcgetattr( PortHandle, &DeviceAttributes);
  if (rc) {
    UsbSerialClose( PortHandle);
    ErrorLogLn( "UsbSerialOpen('%s') tcgetattr rc %d", SerialDevice, rc);
    return false;
  }
  DeviceAttributes.c_iflag = 0;
  DeviceAttributes.c_oflag = 0;
  DeviceAttributes.c_cflag = CREAD | CS8 | CLOCAL; // enable receiver, 8n1, no hardware handshaking
  DeviceAttributes.c_lflag = 0;
  cfsetospeed( &DeviceAttributes, BaudRateCode); // output baud rate
  cfsetispeed( &DeviceAttributes, BaudRateCode); //  input baud rate
  if (rc = tcsetattr( PortHandle, TCSANOW, &DeviceAttributes)) {
    UsbSerialClose( PortHandle);
    ErrorLogLn( "UsbSerialOpen('%s') tcsetattr rc %d", SerialDevice, rc);
    return false;
  }
  return true;
}

static bool UsbSerialGet( int PortHandle, uint8_t & RecvByte, int TimeoutMs) {
  if (read( PortHandle, &RecvByte, 1) == 1) {
    return true;
  }
  if (TimeoutMs) {
    UtilSleep_mS( TimeoutMs);
    return UsbSerialGet( PortHandle, RecvByte, 0);
  }
  RecvByte = 0;
  return false;
}

//----------------------------------------------------------------------------
// Hex dump a binary buffer.
//----------------------------------------------------------------------------

static void HexDump( ccptr Tag, bptr Data, uint16_t Length) {
  if (Data && Length) {
    char logline[100];
    char logline2[100];
    snprintf(logline, 100, "%s [%d]: ", Tag, Length);
    for (uint16_t i = 0; i < Length; i++) {
      if (i % 32 == 0) {
        QmodemLogLn( logline);
        strncpy(logline, "", 100);
      }
      strncpy( logline2, logline, 100);
      snprintf( logline, 100, "%s, %2.2x", logline2, Data[i]);
    }
    QmodemLogLn( logline);
} }

//----------------------------------------------------------------------------
// Calculate the 16-bit crc of a message.  Given a message m of length n in a
// buffer of length n+3, the crc is calculated over m[0]..m[n-1], then m[n] is
// filled in with the lsb of the crc, m[n+1] with the msb, and m[n+2] with
// 0x7e, the message terminator.
//----------------------------------------------------------------------------

static uint16_t Crc16( bptr Data, uint16_t Length) {
  uint32_t NewCrc;
  uint8_t Byte;
  int32_t BitLength;
  NewCrc = 0x00F32100;
  while (Length--) {
    Byte = *Data++;
    for (BitLength = 0; BitLength <= 7; BitLength++) {
      NewCrc >>= 1;
      if (Byte & 0x01) {
        NewCrc |= 0x00800000;
      }
      if (NewCrc & 0x00000080) {
        NewCrc ^= 0x00840800;
      }
      Byte >>= 1;
  } }
  for (BitLength = 0; BitLength <= 15; BitLength++) {
    NewCrc >>= 1;
    if (NewCrc & 0x00000080) {
      NewCrc ^= 0x00840800;
  } }
  return (~NewCrc) >> 8;
}

static uint8_t  RecvData[1024];
static uint16_t RecvLength = 0;
static bool  RecvEscape = false;

static void RecvFlush( void) {
  RecvLength = 0;
  RecvEscape = false;
}

//-----------------------------------------------------------------------------
// Franklin/Icera Serial Device Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-uint8_t limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool FwPutBuffer( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static char EscData[256];
    int EscLength = 0;
    for (int i = 0; i < RawLength; i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      UtilSleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Return true if Data is a valid message with correct crc and a
// frame end mark.

static bool FwMsgCheck( bptr RecvData, uint16_t RecvLength) {
  uint16_t crc= Crc16( RecvData, RecvLength-3);
  uint8_t CrcLo = crc & 0xff;
  uint8_t CrcHi = crc >> 8;
  uint8_t Frame = 0x7e;
  if (RecvData[RecvLength-3] != CrcLo) return false;
  if (RecvData[RecvLength-2] != CrcHi) return false;
  if (RecvData[RecvLength-1] != Frame) return false;
  return true;
}

// Put a uint8_t into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5d -> 7d
//    7d 5e -> 7e

#define FwRspLen_Dbm 31 // includes crc/eof
#define FwRspOff_Dbm 18

static bool FwGetByte( uint8_t RecvByte, bptr XmitData, uint16_t XmitLength) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvLength < sizeof( RecvData)) {
    RecvData[RecvLength++] = RecvByte;
  }
  if (RecvByte == 0x7e) {
    HexDump( "Xmit1", XmitData, XmitLength);
    HexDump( "Recv1", RecvData, RecvLength);
    if (FwMsgCheck( RecvData, RecvLength)) {
      switch (RecvLength) {
        case FwRspLen_Dbm:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            uint16_t Dbm = RecvData[FwRspOff_Dbm];
            rc = ExportDbm( Dbm);
          }
          break;
    } }
    RecvFlush();
  }
  return rc;
}

// Xmit a message after calculating and appending the crc.  The
// FwPutBuffer routine will escape and frame it.

static bool FwMsgXmit( int Modem, bptr XmitData, uint16_t XmitLength) {
  static uint8_t CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  uint16_t crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = crc & 0xff;
  CrcData[XmitLength++] = crc >> 8;
  return FwPutBuffer( Modem, CrcData, XmitLength);
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool FwMsgRecv( int Modem, bptr XmitData, uint16_t XmitLength) {
  uint8_t RecvByte;
  RecvFlush();
  while (UsbSerialGet( Modem, RecvByte, 10)) {
    if (FwGetByte( RecvByte, XmitData, XmitLength)) {
      return true;
  } }
  return false;
}

// Discard all receive Data, then send a snapshot and interpret any response.

static uint8_t FwGetDbm[] = { // 18 bytes
  0x00, 0x0b, 0x00, 0x0d, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x05, 0x00, 0x01, 0x7c, 0xe4, 0x07
};

static bool FwQuery( int Modem, bptr XmitData, int XmitLength) {
  uint8_t RecvByte;
  while (UsbSerialGet( Modem, RecvByte, 10)) ;
  if (FwMsgXmit( Modem, XmitData, XmitLength)) {
    return FwMsgRecv( Modem, XmitData, XmitLength);
  }
  return false;
}

//-----------------------------------------------------------------------------
// Franklin/Icera Interface/Endpoint Support
//-----------------------------------------------------------------------------

// Skip any initial frame delimiters and verify the presence of a terminal
// frame delimiter.  Return true with successfully unframed message at start
// of buffer (in-place) and unframed message length.

static bool FwEpMsgFrameCheck( bptr RecvData, uint16_t & RecvLength) {
  if (RecvData) {
    bptr RawData = RecvData;
    uint16_t RawLength = RecvLength;
    RecvLength = 0;
    while (RawLength--) {
      uint8_t RawByte = *RawData++;
      if (RawByte == 0x7e) {
        if (RecvLength) return true;
      } else {
        RecvData[RecvLength++] = RawByte;
  } } }
  return false;
}

// Unescape a buffer in-place and adjust the message length.  The unescape
// transformations are:
//
//    7d 5d -> 7d
//    7d 5e -> 7e

static void FwEpMsgDecode( bptr RecvData, uint16_t & RecvLength) {
  if (RecvData) {
    uint16_t EncodeLength = RecvLength;
    bptr EncodeData = RecvData;
    RecvLength = 0;
    while (EncodeLength--) {
      uint8_t EncodeByte = *EncodeData++;
      if (EncodeByte == 0x7d) {
        if (EncodeLength) {
          EncodeLength--;
          EncodeByte = *EncodeData++;
          if (EncodeByte == 0x5d) RecvData[RecvLength++] = 0x7d;
          if (EncodeByte == 0x5e) RecvData[RecvLength++] = 0x7e;
        }
      } else {
        RecvData[RecvLength++] = EncodeByte;
} } } }

// Return true and adjust the message length if the crc is correct.

static bool FwEpMsgCrcCheck( bptr RecvData, uint16_t & RecvLength) {
  if (RecvData && (RecvLength > 2)) {
    uint16_t Crc = Crc16( RecvData, RecvLength-2);
    uint8_t CrcLo = Crc & 0xff;
    uint8_t CrcHi = Crc >> 8;
    if ((RecvData[RecvLength-2] == CrcLo) &&
        (RecvData[RecvLength-1] == CrcHi)) {
      RecvLength -= 2; // discard crc
      return true;
  } }
  return false;
}

// Expected messages for various modem types.

#define FwEpRspLen301_Dbm 15 // 15 bytes, unescaped, without crc or framing
#define FwEpRspOff301_Dbm  5

#define FwEpRspLen600_Dbm 28 // 28 bytes, unescaped, without crc or framing
#define FwEpRspOff600_Dbm 18

// Skipping any initial frame delimiter, then unescape and check the crc.

static bool FwEpMsgRecv( int EndpointRecv, bptr XmitData) {
  bool rc = false;
  static uint8_t RecvData[128];
  int RecvMax = sizeof( RecvData);
  uint16_t RecvLength = usb_bulk_read( UsbDevHandle, EndpointRecv, (cptr) RecvData, RecvMax, 100);
  if (RecvLength > 0) {
    if (FwEpMsgFrameCheck( RecvData, RecvLength)) {
      FwEpMsgDecode( RecvData, RecvLength);
      if (FwEpMsgCrcCheck( RecvData, RecvLength)) {
        HexDump( "Recv2", RecvData, RecvLength);
        switch (RecvLength) {
          case FwEpRspLen301_Dbm:
            if (memcmp( RecvData, XmitData, 1) == 0) {
              uint16_t Dbm = RecvData[FwEpRspOff301_Dbm];
              rc = ExportDbm( Dbm);
            }
            break;
          case FwEpRspLen600_Dbm:
            if (memcmp( RecvData, XmitData, 4) == 0) {
              uint16_t Dbm = RecvData[FwEpRspOff600_Dbm];
              rc = ExportDbm( Dbm);
            }
            break;
  } } } }
  return rc;
}

// Escape a buffer, bracket it with frame delimiters, and send it.

static bool FwEpPutBuffer( int EndpointXmit, bptr XmitData, uint16_t XmitLength) {
  static uint8_t EncodeData[256];
  uint16_t EncodeLength = 0;
  EncodeData[EncodeLength++] = 0x7e;
  for (int i = 0; i < XmitLength; i++) {
    switch (XmitData[i]) {
      case 0x7d: EncodeData[EncodeLength++] = 0x7d; EncodeData[EncodeLength++] = 0x5d; break;
      case 0x7e: EncodeData[EncodeLength++] = 0x7d; EncodeData[EncodeLength++] = 0x5e; break;
      default  : EncodeData[EncodeLength++] = XmitData[i];
  } }
  EncodeData[EncodeLength++] = 0x7e;
  int XmitRc = usb_bulk_write( UsbDevHandle, EndpointXmit, (cptr) EncodeData, EncodeLength, 0);
  if (XmitRc == EncodeLength) {
    return true;
  } else {
    ErrorLogLn( "Qmodem - FwEpPutBuffer bulk write returned %d (expected %d)", XmitRc, EncodeLength);
  }
  return false;
}

// Xmit a message after calculating and appending the crc.  The
// FwEpPutBuffer routine will escape and frame it.

static bool FwEpMsgXmit( int EndpointXmit, bptr XmitData, uint16_t XmitLength) {
  HexDump( "Xmit3", XmitData, XmitLength);
  static uint8_t CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  uint16_t Crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = Crc & 0xff;
  CrcData[XmitLength++] = Crc >> 8;
  return FwEpPutBuffer( EndpointXmit, CrcData, XmitLength);
}

// Query the signal strength via a specific usb endpoint.  For the moment, we
// assume the endpoint is bidirectional, and derive the recv address from the
// xmit address.

static bool FwEpQuery( int Interface, int Endpoint, bptr XmitData, uint16_t XmitLength) {
  bool rc = false;
  int err;
  if (Interface < 0) {
    QmodemLogLn( "  Using endpoint %d.", Endpoint);
  } else {
    QmodemLogLn( "  Using endpoint %d on interface %d.", Endpoint, Interface);
    usb_detach_kernel_driver_np( UsbDevHandle, Interface); // ignore errors
    if (err = usb_claim_interface( UsbDevHandle, Interface)) {
      ErrorLogLn( "Qmodem - FwEpQuery claim interface error %d", err);
  } }
  int EndpointXmit = Endpoint       ;
  int EndpointRecv = Endpoint | 0x80;
  usb_clear_halt( UsbDevHandle, EndpointXmit);
  usb_clear_halt( UsbDevHandle, EndpointRecv);
  if (FwEpMsgXmit( EndpointXmit, XmitData, XmitLength)) {
    rc = FwEpMsgRecv( EndpointRecv, XmitData);
  }
  if (Interface >= 0) {
    if (err = usb_release_interface( UsbDevHandle, Interface)) {
      ErrorLogLn( "Qmodem - FwEpQuery release interface error %d", err);
  } }
  return rc;
}

// Query messages for various modem types.

static uint8_t FwEpGetDbm301[] = { // 2 bytes, unescaped, without crc or framing
  0xcc, 0x00
};

static uint8_t FwEpGetDbm600[] = { // 18 bytes, unescaped, without crc or framing
  0x00, 0x0b, 0x00, 0x0d, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x05, 0x00, 0x01, 0x7c, 0xe4, 0x07
};

//-----------------------------------------------------------------------------
// Qualcomm/Novatel Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-uint8_t limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool NtPutBuffer( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static char EscData[256];
    int EscLength = 0;
    for (int i = 0; (i < RawLength) && (EscLength < 250); i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      UtilSleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Return true if Data is a valid message with correct crc and a
// frame end mark.

static bool NtMsgCheck( bptr RecvData, uint16_t RecvLength) {
  if (RecvLength <= 3) return false;
  uint16_t crc= Crc16( RecvData, RecvLength-3);
  uint8_t CrcLo = crc & 0xff;
  uint8_t CrcHi = crc >> 8;
  uint8_t Frame = 0x7e;
  if (RecvData[RecvLength-3] != CrcLo) return false;
  if (RecvData[RecvLength-2] != CrcHi) return false;
  if (RecvData[RecvLength-1] != Frame) return false;
  return true;
}

// Put a uint8_t into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5e -> 7e
//    7d 5d -> 7d

#define NtRspLen_Rssi 116 // includes crc/eof
#define NtRspOff_Rssi  13

#define NtRspLen_Dbm   15 // includes crc/eof
#define NtRspOff_Dbm   10

static bool NtGetByte( uint8_t RecvByte, bptr XmitData, uint16_t XmitLength) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvLength < sizeof( RecvData)) {
    RecvData[RecvLength++] = RecvByte;
  }
  if (RecvByte == 0x7e) {
    HexDump( "Xmit4", XmitData, XmitLength);
    HexDump( "Recv4", RecvData, RecvLength);
    if (NtMsgCheck( RecvData, RecvLength)) {
      switch (RecvLength) {
        case NtRspLen_Rssi:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            uint16_t Csq = RecvData[NtRspOff_Rssi];
            rc = ExportCsq( Csq);
          }
          break;
        case NtRspLen_Dbm:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            uint16_t Dbm = RecvData[NtRspOff_Dbm];
            rc = ExportDbm( Dbm);
          }
    } }
    RecvFlush();
  }
  return rc;
}

// Xmit a message after calculating and appending the crc.  The
// NtPutBuffer routine will escape and frame it.

static bool NtMsgXmit( int Modem, bptr XmitData, uint16_t XmitLength) {
  static uint8_t CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  uint16_t crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = crc & 0xff;
  CrcData[XmitLength++] = crc >> 8;
  return NtPutBuffer( Modem, CrcData, XmitLength);
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool NtMsgRecv( int Modem, bptr XmitData, uint16_t XmitLength) {
  uint8_t RecvByte;
  RecvFlush();
  while (UsbSerialGet( Modem, RecvByte, 10)) {
    if (NtGetByte( RecvByte, XmitData, XmitLength)) {
      return true;
  } }
  return false;
}

// Qualcomm/Novatel QCDM protocol values (for documentation only)

#define QualcommDmSubSys 0x4b

#define NtChipset6800  0xfa
#define NtChipset6500  0x32

#define NtSnapshotSubCmd  0x0007

#define NtEvdo   0x07
#define NtWcdma  0x14

// Qualcomm/Novatel snapshot query messages for chipset/technology combinations:

static uint8_t NtGetRssiEv65[] = { 0x4b, 0x32, 0x07, 0x00, 0x07, 0xff, 0xff };
static uint8_t NtGetRssiWc65[] = { 0x4b, 0x32, 0x07, 0x00, 0x14, 0xff, 0xff };
static uint8_t NtGetRssiEv68[] = { 0x4b, 0xfa, 0x07, 0x00, 0x07, 0xff, 0xff };
static uint8_t NtGetRssiWc68[] = { 0x4b, 0xfa, 0x07, 0x00, 0x14, 0xff, 0xff };

static uint8_t NtGetDbm[142] = {
  0x4b, 0xfa, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf1
};

// Discard all receive Data, then send a snapshot and interpret any response.

static bool NtQuery( int Modem, bptr XmitData, int XmitLength) {
  uint8_t RecvByte;
  while (UsbSerialGet( Modem, RecvByte, 10)) ;
  if (NtMsgXmit( Modem, XmitData, XmitLength)) {
    return NtMsgRecv( Modem, XmitData, XmitLength);
  }
  return false;
}

//-----------------------------------------------------------------------------
// Pantech UML290 Support
//-----------------------------------------------------------------------------

static uint8_t PtSwitchto3G[21] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x7d, 0x24, 0x7d, 0x21, 0x7d, 0x21, 0x7d, 0x20,
  0x7d, 0x20, 0xce, 0xed, 0x0d
};

static uint8_t PtSwitchto4G[21] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x7d, 0x24, 0x7d, 0x21, 0x7d, 0x34, 0x7d, 0x20,
  0x7d, 0x20, 0xe6, 0x51, 0x0d
};

static uint8_t PtSwitchPart2[22] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x24, 0x7d, 0x23, 0x7d, 0x20, 0x7d, 0x20, 0x7d,
  0x20, 0x7d, 0x21, 0x3d, 0x41, 0x0d
};

static uint8_t PtSwitchPart3[14] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x27, 0x7d, 0x20, 0x3c, 0x93, 0x0d
};

static uint8_t PtSwitchPart4[14] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x27, 0x7d, 0x21, 0xb5, 0x82, 0x0d
};

static bool PtSwitch( int Modem, bptr XmitData, int XmitLength) {
  uint8_t RecvByte;
  while (UsbSerialGet( Modem, RecvByte, 10)) ;
  return write( Modem, XmitData, XmitLength);
}

//-----------------------------------------------------------------------------
// Sierra Wireless CnS Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-uint8_t limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool SwMsgXmit( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static uint8_t EscData[256];
    int EscLength = 0;
    EscData[EscLength++] = 0x7e;
    for (int i = 0; i < RawLength; i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    HexDump( "Xmit5", EscData+1, EscLength-2);
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      UtilSleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Put a uint8_t into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5e -> 7e
//    7d 5d -> 7d

#define SwBit16(x,y) ((x[y] << 8) | x[y+1])

// Supported cns object id codes

#define SwGetRssi   0x1001
#define SwGetActive 0x1009
#define SwGetEcio   0x1065

// Expected cns payload lengths

#define SwLenRssi   2
#define SwLenActive 2
#define SwLenEcio   8

static bool SwGetByte( uint8_t RecvByte, uint16_t CnsObjId, uint32_t CnsAppId) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvByte == 0x7e) {
    if (RecvLength > 12) {
      HexDump( "Recv6", RecvData, RecvLength);
      uint16_t CnsLength = SwBit16( RecvData, 0), HipLength = 0, PayLength = 0;
      if ((CnsObjId == SwGetRssi  ) && (CnsLength == 10 + SwLenRssi  )) { HipLength = CnsLength + 4; PayLength = SwLenRssi  ; }
      if ((CnsObjId == SwGetActive) && (CnsLength == 10 + SwLenActive)) { HipLength = CnsLength + 4; PayLength = SwLenActive; }
      if ((CnsObjId == SwGetEcio  ) && (CnsLength == 10 + SwLenEcio  )) { HipLength = CnsLength + 4; PayLength = SwLenEcio  ; }
      if (RecvLength == HipLength) {
        if ((RecvData[ 2] ==                     0x6b ) && // hip host notification
            (RecvData[ 3] ==                     0x00 ) && // hip reserved
            (RecvData[ 4] == ((CnsObjId >>  8) & 0xff)) &&
            (RecvData[ 5] == ( CnsObjId        & 0xff)) &&
            (RecvData[ 6] ==                     0x02 ) && // cns reply
            (RecvData[ 7] ==                     0x00 ) && // cns reserved
            (RecvData[ 8] == ((CnsAppId >> 24) & 0xff)) &&
            (RecvData[ 9] == ((CnsAppId >> 16) & 0xff)) &&
            (RecvData[10] == ((CnsAppId >>  8) & 0xff)) &&
            (RecvData[11] == ( CnsAppId        & 0xff)) &&
            (RecvData[12] ==                     0x00 ) &&
            (RecvData[13] ==                PayLength )) {
          bptr CnsData = RecvData+14;
          switch (CnsObjId) {
            case SwGetRssi  : rc = SwExportRssi  ( SwBit16( CnsData, 0)); break;
            case SwGetActive: rc = SwExportActive( SwBit16( CnsData, 0)); break;
            case SwGetEcio  : rc = SwExportEcio  ( SwBit16( CnsData, 0),
                                                   SwBit16( CnsData, 2),
                                                   SwBit16( CnsData, 4),
                                                   SwBit16( CnsData, 6)); break;
    } } } }
    RecvFlush();
  } else {
    if (RecvLength < sizeof( RecvData)) {
      RecvData[RecvLength++] = RecvByte;
  } }
  return rc;
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool SwMsgRecv( int Modem, uint16_t CnsObjId, uint32_t CnsAppId) {
  uint8_t RecvByte;
  RecvFlush();
  while (UsbSerialGet( Modem, RecvByte, 10)) {
    if (SwGetByte( RecvByte, CnsObjId, CnsAppId)) {
      return true;
  } }
  return false;
}

static bool SwQuery( int Modem, uint16_t CnsObjId) {
  static uint8_t HipData[50];
  uint32_t CnsAppId = time( NULL) ^ CnsObjId;
  uint8_t RecvByte;
  uint16_t HipLength = 0;

  HipData[HipLength++] = 0x00; // hip length placeholder
  HipData[HipLength++] = 0x00; // hip length placeholder
  HipData[HipLength++] = 0x2b; // hip modem notification
  HipData[HipLength++] = 0x00; // hip reserved

  uint16_t CnsOffset = HipLength;

  HipData[HipLength++] = (CnsObjId >>   8) & 0xff;
  HipData[HipLength++] =  CnsObjId         & 0xff;
  HipData[HipLength++] = 0x01; // cns get
  HipData[HipLength++] = 0x00; // cns reserved
  HipData[HipLength++] = (CnsAppId >> 24) & 0xff;
  HipData[HipLength++] = (CnsAppId >> 16) & 0xff;
  HipData[HipLength++] = (CnsAppId >>  8) & 0xff;
  HipData[HipLength++] =  CnsAppId        & 0xff;
  HipData[HipLength++] = 0x00; // cns reserved
  HipData[HipLength++] = 0x00; // cns parameter length

  uint16_t CnsLength = HipLength - CnsOffset;

  HipData[0] = (CnsLength >> 8) & 0xff;
  HipData[1] =  CnsLength       & 0xff;

  while (UsbSerialGet( Modem, RecvByte, 10)) ;
  if (SwMsgXmit( Modem, HipData, HipLength)) {
    return SwMsgRecv( Modem, CnsObjId, CnsAppId);
  }
  return false;
}

//-----------------------------------------------------------------------------
// Determine which type of modem is installed, and potentially override the
// default UsbPort value.
//-----------------------------------------------------------------------------

#define SwCdmaCns 1

#define NtCdmaDm  2

#define FwCdma301 31
#define FwCdma600 32

#define PtHybrid  41
#define PtUml290  42

static int GetModemType( ccptr & UsbPort) {
  if (UtilStrCmp( ActiveType, "sw250") == 0)                             return SwCdmaCns;
  if (UtilStrCmp( ActiveType, "sw597") == 0)                             return SwCdmaCns;
  if (UtilStrCmp( ActiveType, "sw598") == 0)                             return SwCdmaCns;
  if (UtilStrCmp( ActiveType, "hw366") == 0) { UsbPort = "/dev/ttyUSB0"; return SwCdmaCns; }
  if (UtilStrCmp( ActiveType, "nt727") == 0)                             return NtCdmaDm ;
  if (UtilStrCmp( ActiveType, "nt760") == 0)                             return NtCdmaDm ;
  if (UtilStrCmp( ActiveType, "fw301") == 0) { UsbPort = "/dev/ttyUSB2"; return FwCdma301; }
  if (UtilStrCmp( ActiveType, "fw600") == 0) { UsbPort = "/dev/ttyUSB2"; return FwCdma600; }
  if (UtilStrCmp( ActiveType, "pt290") == 0) { UsbPort = "/dev/ttyUSB2"; return PtUml290 ; }
  if (UtilStrCmp( ActiveType, "pt190") == 0) { UsbPort = "/dev/ttyACM0"; return PtHybrid ; }
  if (UtilStrCmp( ActiveType, "pt185") == 0) { UsbPort = "/dev/ttyACM0"; return PtHybrid ; }
  if (UtilStrCmp( ActiveType, "nt551") == 0) { UsbPort = "/dev/ttyUSB0"; return PtHybrid ; }
//if (UtilStrCmp( ActiveType, "fw301") == 0)                             return NtCdmaDm ;
  return 0;
}

//-----------------------------------------------------------------------------
// Identify a usb device channel with the target pid/vid and open it.  This is
// the initial call when using libusb.  Returns a non-NULL device handle on
// success.
//-----------------------------------------------------------------------------

static bool LibUsbSeek( int Vid, int Pid) {
  usb_init();
  usb_find_busses();
  usb_find_devices();
  QmodemLogLn( "  Scanning for Vid:%4.4x Pid:%4.4x...", Vid, Pid);
  for (struct usb_bus * bus = usb_get_busses(); bus; bus = bus->next) {
    for (struct usb_device * dev = bus->devices; dev; dev = dev->next) {
      if ((dev->descriptor.idVendor == Vid) && (dev->descriptor.idProduct == Pid)) {
        UsbDevice = dev;
        QmodemLogLn( "  Device found");
  } } }
  if (UsbDevice) {
    if (UsbDevHandle = usb_open( UsbDevice)) {
      return true;
    }
    ErrorLogLn( "Qmodem - LibUsbSeek device open failed");
  } else {
    ErrorLogLn( "Qmodem - LibUsbSeek device not found");
  }
  return false;
}

//-----------------------------------------------------------------------------
// Given a hex character (mixed case), return a 4-bit value 0..15 or -1 for
// error (hex digit invalid).
//-----------------------------------------------------------------------------

static int HexNibble( char x) {
  if (x) {
    if (strchr( "abcdef"    , x)) return x - 'a' + 10;
    if (strchr( "ABCDEF"    , x)) return x - 'A' + 10;
    if (strchr( "0123456789", x)) return x - '0'     ;
  }
  return -1;
}

//-----------------------------------------------------------------------------
// Given an unsigned hex string (mixed case, with optional leading whitespace
// and 0x prefix), return a 16-bit integer value from 0..65536 or -1 for
// error (leading hex digit invalid or null pointer).
//-----------------------------------------------------------------------------

static int HexBit16( ccptr x) {
  if (x) {
    while (*x == ' ') x++;
    int Nibble, Value = HexNibble( *x++);
    if (strncmp( x, "0x", 2) == 0) x += 2;
    if (Value >= 0) {
      do {
        Nibble = HexNibble( *x++);
        if (Nibble >= 0) {
          Value <<= 4;
          Value += Nibble;
        }
      } while (Nibble >= 0);
      return Value;
  } }
  return -1;
}

//-----------------------------------------------------------------------------
// Return the VID (vendor id) of the modem installed or -1 on error.
//-----------------------------------------------------------------------------

static int GetModemVid( void) {
  if (strlen( ActiveVid) == 4) {
    return HexBit16( ActiveVid);
  }
  return -1;
}

//-----------------------------------------------------------------------------
// Return the PID (product id) of the modem installed or -1 on error.
//-----------------------------------------------------------------------------

static int GetModemPid( void) {
  if (strlen( ActivePid) == 4) {
    return HexBit16( ActivePid);
  }
  return -1;
}

static int ModemVid = -1;
static int ModemPid = -1;

//-----------------------------------------------------------------------------
// Try appropriate query commands for different modems, chipsets and wireless
// technologies.  Return true if the rssi values are updated.
//-----------------------------------------------------------------------------

static bool Qmodem2( void) {
  QmodemLogLn( "Qmodem %s", UtilDateTime());
  int rc = 1; // resume error until proven successful
  ccptr UsbPort = "/dev/ttyUSB1";
  int ModemType = GetModemType( UsbPort);
  if (ModemType) {

    // If we have one of the Franklin modems, switch to endpoint mode
    // automatically.

    switch( ModemType) {
      case FwCdma301:
      case FwCdma600:
        ModemVid = GetModemVid();
        ModemPid = GetModemPid();
        if ((ModemVid >= 0) && (ModemPid >= 0)) {
          LibUsbSeek( ModemVid, ModemPid); // sets UsbDevHandle on success
        }
        break;
    }

    // Do the query in either endpoint mode or serial device mode.

    if (UsbDevHandle) {
      QmodemLogLn( "Endpoint mode)");
      switch( ModemType) {
        case FwCdma301:
          QmodemLogLn( "Franklin 301");
          if (rc && FwEpQuery(  2, 4, FwEpGetDbm301, sizeof( FwEpGetDbm301))) rc = 0;
          break;
        case FwCdma600:
          QmodemLogLn( "Franklin 600)");
          if (rc && FwEpQuery( -1, 9, FwEpGetDbm600, sizeof( FwEpGetDbm600))) rc = 0;
          break;
      }
      usb_close( UsbDevHandle);
    } else {
      int ModemPort = 0;
      if (UsbSerialOpen( ModemPort, UsbPort)) {
        QmodemLogLn( "UsbSerialOpen('%s') ModemPort %d", UsbPort, ModemPort);
        switch( ModemType) {
          case NtCdmaDm:
            QmodemLogLn( "Novatel");
            if (rc && NtQuery( ModemPort, NtGetRssiEv68, sizeof( NtGetRssiEv68))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiWc68, sizeof( NtGetRssiWc68))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiEv65, sizeof( NtGetRssiEv65))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiWc65, sizeof( NtGetRssiWc65))) rc = 0;
            if (rc) {
              if (NtQuery( ModemPort, NtGetDbm, sizeof( NtGetDbm))) rc = 0;
            } else {
                  NtQuery( ModemPort, NtGetDbm, sizeof( NtGetDbm));
            }
            break;
          case SwCdmaCns:
            QmodemLogLn( "Sierra");
            if (SwQuery( ModemPort, SwGetActive)) rc = 0;
            if (SwQuery( ModemPort, SwGetRssi  )) rc = 0;
            if (SwQuery( ModemPort, SwGetEcio  )) rc = 0;
            break;
          case FwCdma301:
          case FwCdma600:
            QmodemLogLn( "Franklin");
            if (FwQuery( ModemPort, FwGetDbm, sizeof( FwGetDbm))) rc = 0;
            break;
          case PtUml290:
            QmodemLogLn( "Pantech");
              // TO-DO:  Find a way to get qmodem to either perform a switch to 3G or 4G
              // based on what mode the modem is currently in
            { ccptr Mode = "";
              if (UtilStrCmp( Mode, "3g") == 0) {
                if (PtSwitch( ModemPort, PtSwitchto3G,  sizeof( PtSwitchto3G)))  rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart2, sizeof( PtSwitchPart2))) rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart3, sizeof( PtSwitchPart3))) rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart4, sizeof( PtSwitchPart4))) rc = 0;
              }
              if (UtilStrCmp( Mode, "4g") == 0) {
                if (PtSwitch( ModemPort, PtSwitchto4G,  sizeof( PtSwitchto4G)))  rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart2, sizeof( PtSwitchPart2))) rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart3, sizeof( PtSwitchPart3))) rc = 0;
                if (PtSwitch( ModemPort, PtSwitchPart4, sizeof( PtSwitchPart4))) rc = 0;
            } }
            break;
        }
        UsbSerialClose( ModemPort);
      } else {
        ErrorLogLn( "Qmodem - error opening [%s]", UsbPort);
    } }
  } else {
    ErrorLogLn( "Qmodem - unrecognized modem");
  }
  if (rc == 0 && (ModemType != PtUml290)) {
    ExportCsqDbmSignal();
    UtilFileDelete( TMP_VALUE_RSSI_ERR);
    if (UtilFileSet( TMP_VALUE_RSSI_SIG, SigRc))
      UtilFlagForce( TMP_FLAG_RSSI_UPDATE);
  } else if (!rc)
    UtilFileDelete( TMP_FLAG_RSSI_UPDATE);
  return rc ? false : true;
}

// Return true if rssi values are updated.

bool Qmodem( void) {
  bool Success = false;
  if (int rc = MutexObtain( "qmodem")) {
    ErrorLogLn( "MutexObtain('qmodem') rc %d", rc);
  } else {
    Success = Qmodem2();
    MutexRelease();
  }
  return Success;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

