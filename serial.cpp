//--------------------------------------------------------------------------
// Module: Serial Driver Implementation                      +------------+
// Author: Michael Nagy                                      | serial.cpp |
// Date:   15-Aug-2012                                       +------------+
//--------------------------------------------------------------------------

#include "main.h"

bool SerialOpen( int & PortHandle, ccptr SerialDevice, int OpenOptions) {
  if (PortHandle) {
    ErrorLogLn( "SerialOpen('%s') bad handle %d", SerialDevice, PortHandle);
    return false;
  }
  PortHandle = open( SerialDevice, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (PortHandle <= 0) {
    PortHandle = 0;
    ErrorLogLn( "SerialOpen('%s') open %d", SerialDevice, PortHandle);
    return false;
  }
  int BaudRateCode = B115200;
  if (OpenOptions & SERIALOPEN_9600) {
    BaudRateCode = B9600;
  }
  struct termios DeviceAttributes;
  int rc = tcgetattr( PortHandle, &DeviceAttributes);
  if (rc) {
    SerialClose( PortHandle);
    ErrorLogLn( "SerialOpen('%s') tcgetattr rc %d", SerialDevice, rc);
    return false;
  }
  if (OpenOptions & SERIALOPEN_COOKED) {
    DeviceAttributes.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|INPCK|ISTRIP|INLCR|IGNCR|ICRNL|IXON|IXOFF);
    DeviceAttributes.c_iflag |= IGNPAR; // input modes
    DeviceAttributes.c_oflag &= ~(OPOST|ONLCR|OCRNL|ONOCR|ONLRET|OFILL|OFDEL|NLDLY|CRDLY|TABDLY|BSDLY|VTDLY|FFDLY);
    DeviceAttributes.c_oflag |= NL0|CR0|TAB0|BS0|VT0|FF0; // output modes
    DeviceAttributes.c_cflag &= ~(CSIZE|PARENB|PARODD|HUPCL|CRTSCTS);
    DeviceAttributes.c_cflag |= CREAD|CS8|CSTOPB|CLOCAL; // control modes
    DeviceAttributes.c_lflag &= ~(ISIG|ICANON|IEXTEN|ECHO);
    DeviceAttributes.c_lflag |= NOFLSH; // local modes
  } else {
    DeviceAttributes.c_iflag = 0;
    DeviceAttributes.c_oflag = 0;
    DeviceAttributes.c_cflag = CREAD | CS8 | CLOCAL; // enable receiver, 8n1, no hardware handshaking
    DeviceAttributes.c_lflag = 0;
  }
  cfsetospeed( &DeviceAttributes, BaudRateCode); // output baud rate
  cfsetispeed( &DeviceAttributes, BaudRateCode); //  input baud rate
  if (rc = tcsetattr( PortHandle, TCSANOW, &DeviceAttributes)) {
    SerialClose( PortHandle);
    ErrorLogLn( "SerialOpen('%s') tcsetattr rc %d", SerialDevice, rc);
    return false;
  }
  return true;
}

bool SerialGet( int PortHandle, uint8_t & data, int TimeoutMs) {
  bool DidRead = false;
  data = '\0';
  while(TimeoutMs && !(read(PortHandle,&data, 1) == 1)) {
    UtilSleep_mS( 1);
    TimeoutMs--;
  }
  if (data != '\0') {
    DidRead = true;
  }
  return DidRead;
}

bool SerialPut( int PortHandle, uint8_t data, bool Pace) {
  if (PortHandle > 0) {
    if (Pace) {
      UtilSleep_mS( 2);
    }
    int rc = write( PortHandle, &data, 1);
    if (rc == 1) {
      return true;
  } }
  return false;
}

bool SerialPutBuffer( int PortHandle, bptr data, int count, bool Pace) {
  if (PortHandle > 0) {
    if (Pace) {
      for (int i = 0; i < count; i++) {
        if (!SerialPut( PortHandle, data[i], SERIALPUT_PACE)) {
          return false;
      } }
      return true;
    } else {
      if (write( PortHandle, data, count) == count) {
        return true;
  } } }
  return false;
}

bool SerialPutString( int PortHandle, ccptr data, bool Pace) {
  if (PortHandle > 0) {
    int count = strlen( data);
    if (Pace) {
      for (int i = 0; i < count; i++) {
        if (!SerialPut( PortHandle, data[i], SERIALPUT_PACE)) {
          return false;
      } }
      return true;
    } else {
      int rc = write( PortHandle, data, count);
      if (rc == count) {
        return true;
  } } }
  return false;
}

bool SerialClose( int & PortHandle) {
  if (PortHandle > 0) {
    UtilSleep_mS( 20);
    close( PortHandle);
    PortHandle = 0;
    return true;
  }
  PortHandle = 0;
  return false;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
