//--------------------------------------------------------------------------
// Module: QModem Interface                                    +----------+
// Author: Michael Nagy                                        | qmodem.h |
// Date:   15-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

// Qmodem() is non-reentrant, and may block if another process has invoked
// the qmodem utility (from which the mutex and logic was cloned).

bool Qmodem( void);

// Inputs to Qmodem() will be from the files:
//
//    TMP_VALUE_MODEM
//    TMP_VALUE_MODEM_VID
//    TMP_VALUE_MODEM_PID

// Outputs from Qmodem() will be to the files:
//
//    TMP_VALUE_SIGNAL
//    TMP_VALUE_RSSI_CSQ
//    TMP_VALUE_RSSI_DBM
//    TMP_VALUE_RSSI_PCT
//    TMP_VALUE_RSSI_SIG
//    TMP_VALUE_RSSI_ERR

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
