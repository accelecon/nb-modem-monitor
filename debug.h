//--------------------------------------------------------------------------
// Module: Debug Log                                            +---------+
// Author: Michael Nagy                                         | debug.h |
// Date:   15-Aug-2012                                          +---------+
//--------------------------------------------------------------------------

// Define EXISTING_ONLY for production to only write to pre-existing Debug
// and Trace files.

#define EXISTING_ONLY

// Bad stuff, unconditional, timestamped:

void ErrorLogLn( ccptr fmt, ... );

// General debug stuff, not timestamped, conditional on EXISTING_ONLY:

void DebugLogLn( ccptr fmt, ... );

// For qmodem stuff, not timestamped, conditional on EXISTING_ONLY:

void QmodemLogLn( ccptr fmt, ... );

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

