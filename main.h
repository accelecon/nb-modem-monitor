//--------------------------------------------------------------------------
// Module: Project Includes, Types, Constants and Variables      +--------+
// Author: Michael Nagy                                          | main.h |
// Date:   15-Aug-2012                                           +--------+
//--------------------------------------------------------------------------

#define PROGRAM_NAME "modem-monitor" // should agree with Makefile

//--------------------------------------------------------------------------
// System include files
//--------------------------------------------------------------------------

extern "C" {
#include <roxml.h>
};

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <curl/curl.h>

#include <net/if.h> //just for IFNAMSIZ

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <semaphore.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <usb.h>
#include <utime.h>
#include <math.h>
#include <syslog.h>
#include <signal.h>

//--------------------------------------------------------------------------
// Project type definitions
//--------------------------------------------------------------------------

typedef       char *  cptr; //           ASCIIZ string
typedef const char * ccptr; // immutable ASCIIZ string

typedef       uint8_t *  bptr;
typedef       FILE    *  fptr;

// A pointer to a NULL-terminated array of pointers to ASCIIZ strings.  This
// is used to capture multi-line modem responses.

typedef ccptr * Reply_t;

//--------------------------------------------------------------------------
// Project include files
//--------------------------------------------------------------------------

#include "debug.h"
#include "modem.h"
#include "mutex.h"
#include "serial.h"
#include "util.h"
#include "qmodem.h"


//
#define CNTI_MAX_SIZE 32
#define XPATH_MAX_SIZE 128

//--------------------------------------------------------------------------
// Project interface (file system) constants
//--------------------------------------------------------------------------

#define TMP_LOG_MM_ERROR  "/tmp/log.mm_error"
#define TMP_LOG_MM_DEBUG  "/tmp/log.mm_debug"  // existence enables debug logging
#define TMP_LOG_MM_QMODEM "/tmp/log.mm_qmodem" // existence enables qmodem logging
#define TMP_LOG_MM_SYSTEM "/tmp/log.mm_system"

#define TMP_FLAG_RSSI_ERR          "/tmp/flag.rssi_err"
#define TMP_FLAG_RSSI_UPDATE       "/tmp/flag.rssi_update"
#define TMP_FLAG_WIRELESS_2G       "/tmp/flag.wireless_2g"
#define TMP_FLAG_WIRELESS_3G       "/tmp/flag.wireless_3g"
#define TMP_FLAG_WIRELESS_4G       "/tmp/flag.wireless_4g"
#define TMP_FLAG_WIRELESS_UNKNOWN  "/tmp/flag.wireless_unknown"
#define TMP_FLAG_WIRELESS_OFF      "/tmp/flag.wireless_off"
#define TMP_FLAG_CONNECT_FAIL      "/tmp/flag.connect_fail"
#define TMP_FLAG_MODEM_GOOD        "/tmp/flag.modem_good"
#define TMP_FLAG_DUAL_PORT         "/tmp/flag.dual_port"
#define TMP_FLAG_WIMAX_CONNECTED   "/tmp/flag.wimax_connected"
#define TMP_FLAG_CONNECTED         "/tmp/flag.connected"
#define TMP_FLAG_LTE_3G_MODE       "/tmp/flag.lte_3g_mode"
#define TMP_FLAG_SIDNID_UPDATE     "/tmp/flag.sidnid_update"
#define TMP_FLAG_CIDLAC_UPDATE     "/tmp/flag.cidlac_update"

// These flag names need $MODEM suffixes (from TMP_MODEM_LIST above):

#define TMP_FLAG_FAMILY_QMI_     "/tmp/flag.family_qmi.%s"
#define TMP_FLAG_FAMILY_CDMA1_   "/tmp/flag.family_cdma1.%s"
#define TMP_FLAG_FAMILY_CDMA2_   "/tmp/flag.family_cdma2.%s"
#define TMP_FLAG_FAMILY_CDMA3_   "/tmp/flag.family_cdma3.%s"
#define TMP_FLAG_FAMILY_GSM1_    "/tmp/flag.family_gsm1.%s"
#define TMP_FLAG_FAMILY_GSM2_    "/tmp/flag.family_gsm2.%s"
#define TMP_FLAG_FAMILY_GSM3_    "/tmp/flag.family_gsm3.%s"
#define TMP_FLAG_FAMILY_HYBRID_  "/tmp/flag.family_hybrid.%s"
#define TMP_FLAG_FAMILY_HTTP_    "/tmp/flag.family_http.%s"

// These value names need $MODEM suffixes (from TMP_MODEM_LIST above):

#define TMP_VALUE_SCMD_DEV_  "/tmp/value.scmd_dev_%s"
#define TMP_VALUE_PPPD_DEV_  "/tmp/value.pppd_dev_%s"

// Simple dynamic values:

#define TMP_VALUE_RSSI_ERR    "/var/run/modems/%s/rssi_err"
#define TMP_VALUE_RSSI_SIG    "/var/run/modems/%s/rssi_sig"
#define TMP_VALUE_RSSI_CSQ    "/var/run/modems/%s/rssi_csq"
#define TMP_VALUE_RSSI_DBM    "/var/run/modems/%s/rssi_dbm"
#define TMP_VALUE_RSSI_PCT    "/var/run/modems/%s/rssi_pct"
#define TMP_VALUE_RSSI_ECIO   "/var/run/modems/%s/rssi_ecio"
#define TMP_VALUE_RSSI_RSRP   "/var/run/modems/%s/rssi_rsrp"
#define TMP_VALUE_RSSI_RSRQ   "/var/run/modems/%s/rssi_rsrq"
#define TMP_VALUE_RSSI_SINR   "/var/run/modems/%s/rssi_sinr"
#define TMP_VALUE_RSSI_SNR    "/var/run/modems/%s/rssi_snr"
#define TMP_VALUE_RSSI_CID    "/var/run/modems/%s/rssi_cid"
#define TMP_VALUE_RSSI_LAC    "/var/run/modems/%s/rssi_lac"
#define TMP_VALUE_RSSI_SID    "/var/run/modems/%s/rssi_sid"
#define TMP_VALUE_RSSI_NID    "/var/run/modems/%s/rssi_nid"
#define TMP_VALUE_RSSI_BAND   "/var/run/modems/%s/rssi_band"
#define TMP_VALUE_RSSI_RSSI   "/var/run/modems/%s/rssi_rssi"

#define TMP_VALUE_SIGNAL      "/tmp/value.signal"
#define TMP_VALUE_MODEM       "/tmp/value.modem"                   // defines $MODEM suffix
#define TMP_VALUE_MODEM_VID   "/var/run/modems/%s/MODEM_VID"
#define TMP_VALUE_MODEM_PID   "/var/run/modems/%s/MODEM_PID"
#define TMP_VALUE_MODEM_TYPE   "/var/run/modems/%s/MODEM_TYPE"
#define TMP_VALUE_MODEM_MAKER "/var/run/modems/%s/MODEM_MAKER"
#define TMP_VALUE_MODEM_SCMD  "/var/run/modems/%s/MODEM_SCMD_DEV"
#define TMP_VALUE_ERROR_CNTI  "/var/run/modems/%s/error_cnti"
#define TMP_VALUE_MODEM_CNTI  "/var/run/modems/%s/modem_cnti"
#define TMP_VALUE_MODEM_NET   "/var/run/modems/%s/net_dev"
#define TMP_VALUE_CONNECT_TRY "/tmp/value.connect_try"

#define TMP_VALUE_DEFAULT_GW  "/tmp/value.default_gatweay"

// Simple persistent values:

#define FLASH_VALUE_ERROR_RAW  "/flash/value.error_raw"
#define FLASH_CFG_RAT          "/flash/cfg.rat"

#define FLASH_VALUE_RSSI_NWRSSI_ERR  "/flash/value.rssi_nwrssi_err"
#define FLASH_VALUE_RSSI_NWRSSI_OK   "/flash/value.rssi_nwrssi_ok"

//--------------------------------------------------------------------------
// Project variables
//--------------------------------------------------------------------------

extern char ActiveModem[16];
extern char ActiveVid  [ 8];
extern char ActivePid  [ 8];
extern char ActiveType [16];

//--------------------------------------------------------------------------
// HTTP UI OPTIONS
//--------------------------------------------------------------------------
#define HTTP_POST 0
#define HTTP_GET 1
#define HTTP_VALUE( modem, key) HTTP_##modem##_##key
#define HTTP_POSTDATA_MAX_SIZE 128
//Franklin Wireless U770
#define HTTP_U770_PATH_RSSI "rpc.asp"
#define HTTP_U770_PATH_RSSI_METHOD HTTP_POST
#define HTTP_U770_PATH_RAT "rpc.asp"
#define HTTP_U770_PATH_RAT_METHOD HTTP_POST
#define HTTP_U770_PORT 80
#define HTTP_U770_POSTDATA_WIMAX_RSSI "COUNT=1&WWW_SID=&ACTION_1=function&NAME_1=fti_Serv4GObj_update&VALUE_1="
#define HTTP_U770_POSTDATA_RAT        "COUNT=1&WWW_SID=&ACTION_1=function&NAME_1=fti_connobj_update&VALUE_1="
#define HTTP_U770_POSTDATA_LTE_RSSI   "COUNT=1&WWW_SID=&ACTION_1=function&NAME_1=fti_ServLTEObj_update&VALUE_1="
#define HTTP_U770_POSTDATA_3G_RSSI    "COUNT=1&WWW_SID=&ACTION_1=function&NAME_1=fti_3g_update_info&VALUE_1="
#define HTTP_U770_PATH_RSSI_XPATH_4G  "/root/result"
#define HTTP_U770_PATH_RSSI_XPATH_3G  "/root/result[1]"
#define HTTP_U770_PATH_ECIO_XPATH_3G  "/root/result[2]"
//Pantech UML295
#define HTTP_UML295_PATH_RSSI "condata"
#define HTTP_UML295_PATH_RSSI_METHOD HTTP_GET
#define HTTP_UML295_PATH_RAT "condata"
#define HTTP_UML295_PATH_RAT_METHOD HTTP_GET
//The following XPath expressions are incorrect. They should be "/picaso/p-answer/..."
//Our xml library (roxml) is buggy here; if we change and it breaks, try fixing these.
//(the U770 ones are fine)
#define HTTP_UML295_PATH_RAT_XPATH "/p-answer/condata/network/serving/type"
#define HTTP_UML295_PATH_RSSI_XPATH "/p-answer/condata/signal/parameter[@type=\"signal strength\"]/dbm"
#define HTTP_UML295_PATH_ECIO_XPATH "/p-answer/condata/signal/parameter[@type=\"signal to noise ratio\"]/db"
#define HTTP_UML295_PORT 80
//Huawei K3773
#define HTTP_K3773_PORT 80
#define HTTP_K3773_PATH_RSSI "api/monitoring/status"
#define HTTP_K3773_PATH_RSSI_METHOD HTTP_GET
#define HTTP_K3773_PATH_RSSI_XPATH  "/SignalStrength"
#define HTTP_K3773_PATH_RAT "api/monitoring/status"
#define HTTP_K3773_PATH_RAT_METHOD HTTP_GET
#define HTTP_K3773_PATH_RAT_XPATH  "/CurrentNetworkType"

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
