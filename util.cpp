//--------------------------------------------------------------------------
// Module: Utility Functions                                   +----------+
// Author: Michael Nagy                                        | util.cpp |
// Date:   15-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

#include "main.h"

// Assure Value is restricted to the range Min..Max.  If it falls outside
// that range on entry, assign it the value Default.

int UtilRangeCheck( int Min, int Max, int Default, int & Value) {
  if ((Value < Min) || (Value > Max)) {
    Value = Default;
  }
  return Value;
}

// The temporary string cache which records all dynamic string pointers to
// allow cleanup at the end of each processing cycle.

#define TEMPMAX 500

static int TempStrings = 0;
static cptr TempString[TEMPMAX];

// Add a new dynamic string to the temporary string cache.

static cptr TempPush( cptr String) {
  if (String) {
    if (TempStrings == TEMPMAX) {
      ErrorLogLn( "TempPush exceeded %d temp string limit", TEMPMAX);
    } else {
      TempString[TempStrings++] = String;
  } }
  return String;
}

// Allocate a new dynamic string copy of a string and return a pointer
// to the copy.

ccptr TempDup( ccptr String) {
  return TempPush( strdup( String));
}

// Allocate a new dynamic string buffer and return a pointer to the buffer.

static cptr TempAlloc( int Length) {
  return TempPush( (cptr) calloc( 1, Length+1));
}

// Free all dynamic strings in the temporary string cache.

void TempFlush( void) {
  static int HighWater = 0;
  if ((HighWater < TempStrings)) {
    if (HighWater == 0) {
      DebugLogLn( "TempFlush: Temp string limit is %d.", TEMPMAX);
    }
    if (HighWater < TempStrings) {
      HighWater = TempStrings;
    }
    DebugLogLn( "TempFlush: Temp string usage is %d.", TempStrings);
  }
  while (TempStrings) {
    free( TempString[TempStrings-1]);
    TempString[--TempStrings] = NULL;
} }

// Convert a string to a decimal integer, ignoring any non-digit leading
// characters.

int UtilStr2Int( ccptr String) {
  if (String) {
    const char IntChars[] = "0123456789-";
    while (*String && !strchr( IntChars, *String)) {
      String++;
    }
    if (*String) {
      return atoi( String);
  } }
  return 0;
}

// Extract a real number from a string, ignoring any leading non-numeric
// characters.

float UtilStr2Float( ccptr String) {
  if (String) {
    const char RealChars[] = "0123456789-.";
    while (*String && !strchr( RealChars, *String)) {
      String++;
    }
    if (*String) {
      return atof( String);
  } }
  return 0.0;
}

// Return a string representation of an integer.

ccptr UtilInt2Str( int Value) {
  char Temp[24];
  sprintf( Temp, "%d", Value);
  return TempDup( Temp);
}

// Return a string representation of a real number.

ccptr UtilFloat2Str( float Value) {
  char Temp[24];
  sprintf( Temp, "%5.3f", Value);
  return TempDup( Temp);
}

// Return a dynamically string pointer to a copy of the first Length
// characters of String.

ccptr UtilPrefix( ccptr String, int Length) {
  if (String && *String && Length) {
    if (Length > strlen( String)) {
      Length = strlen( String);
    }
    if (Length) {
      if (cptr Substring = TempAlloc( Length)) {
        memcpy( Substring, String, Length);
        return Substring;
  } } }
  return "";
}

// Return a dynamic string pointer to the Index'th field of String (1-based
// index), using Delimiter to separate fields.  Return an empty string (not
// a NULL) if Index is out of range.

ccptr UtilGetField( ccptr String, int Index, char Delimiter) {
  int Length = 0;
  if (String && *String && (Index > 0)) {
    while (*String && Index--) {
      ccptr Next = String;
      while (*Next && (*Next != Delimiter)) {
        Next++;
      }
      if (*Next && Index) {
        String = Next + 1;
      } else {
        if (Index == 0) {
          Length = Next - String;
        } else {
          Length = Index = 0;
  } } } }
  return UtilTrim( UtilPrefix( String, Length));
}

// Issue a command to the shell (busybox) and return up to the first 200 lines
// of its output in Reply.  Each line is limited to 200 characters in length.

bool UtilSystem( Reply_t & Reply, ccptr Command) {
  UtilReplyFree( Reply);
  if (Command) {
    FILE * CommandStdOut = popen(Command, "r");
    if (CommandStdOut) {
      int n = 0;
      if (Reply = (Reply_t) calloc( 202, sizeof( cptr))) {
        char Line[202];
        while ((n < 200) && fgets( Line, 200, CommandStdOut)) {
          Line[200] = 0;
          Reply[n++] = TempDup( Line);
      } }
      pclose( CommandStdOut);
      if (n) {
        return true;
  } } }
  return false;
}

// Free a pointer to an array of ASCIIZ strings.  The strings themselves were all
// allocated via TempDup(), so we don't have to deal with them here.

void UtilReplyFree( Reply_t & Reply) {
  if (Reply) {
    free( Reply);
    Reply = NULL;
} }

// Return true if a string starts with the specified prefix.

bool UtilStartsWith( ccptr String, ccptr Prefix) {
  if (String && Prefix) {
    if (strncmp( String, Prefix, strlen( Prefix)) == 0) {
      return true;
  } }
  return false;
}

// Copy Source to Target, limiting size to TargetSize.

ccptr UtilStrCpy( cptr Target, ccptr Source, int TargetSize) {
  if (Target && Source && (TargetSize >= 0)) {
    strncpy( Target, Source, TargetSize);
    Target[TargetSize-1] = 0;
} }

// Compare String1 and String2.

int UtilStrCmp( ccptr String1, ccptr String2) {
  if (String1 && String2) {
    return strcmp( String1, String2);
  }
  return -99;
}

// Strip leading and trailing whitespace and quotes from a string in-place.

ccptr UtilTrim( ccptr String) {
  if (String && *String) {
    cptr x = (cptr) String, y = (cptr) String;
    bool Prefix = true;
    while (*x = *y++) {
      if (isspace(*x) || iscntrl(*x) || (*x == '"')) {
        if (!Prefix) {
          x++;
        }
      } else {
        Prefix = false;
        x++;
    } }
    *x = 0;
    x = (cptr) String;
    int n = strlen( x);
    while (n && (isspace(x[n-1]) || iscntrl(x[n-1]) || (x[n-1] == '"'))) x[--n] = 0;
  }
  return String;
}

// Sleep for 1+ seconds.

void UtilSleep( int s) {
  sleep( s);
}

// Return true if a non-blank Substring occurs in String.

bool UtilStrStr( ccptr String, ccptr Substring) {
  if (String && Substring) {
    if (*Substring) {
      return strstr( String, Substring) ? true : false;
  } }
  return false;
}

// Sleep from 0 to 999 milliseconds.

void UtilSleep_mS( int ms) {
  if (ms > 999) {
    UtilSleep   ( ms / 1000);
    UtilSleep_mS( ms % 1000);
  } else {
    if (ms) {
      const timespec ts = { 0, ms * 1000000 };
      nanosleep( &ts, NULL);
} } }

// Sleep from 0 to 999 microseconds.

void UtilSleep_uS( int us) {
  if (us > 999) {
    UtilSleep_mS( us / 1000);
    UtilSleep_uS( us % 1000);
  } else {
    if (us) {
      const timespec ts = { 0, us * 1000 };
      nanosleep( &ts, NULL);
} } }

ccptr UtilChangeCase( bool up, ccptr String){
  static char Buffer[200];
  int i = 0;
  while (String && String[i] && (i < 190) && String[i] != '\0') {
    Buffer[i] = up ? toupper(String[i]) : tolower(String[i]);
    i++;
  }
  Buffer[i] = 0;
  return TempDup( Buffer);
}

ccptr UtilUcase( ccptr String) {
  return UtilChangeCase( true, String);
}

ccptr UtilLcase( ccptr String) {
  return UtilChangeCase( false, String);
}

// Some filenames contain %s placeholders for the modem shortname.  For
// those filenames, insert the current value ofGlobalActiveModem.  For
// filenames without %s, this routine has no effect on the filename.

static ccptr DynamicFilename( ccptr Filename) {
  static char DynamicFile[128];
  sprintf( DynamicFile, Filename, ActiveModem);
  return DynamicFile;
}

void  UtilFileCreate( ccptr Filename) {
  if (UtilFileExists( Filename)){
    utime( DynamicFilename( Filename), NULL);
  } else if (fptr f = fopen( DynamicFilename( Filename), "w")) {
    fprintf( f, "\n");
    fclose( f);
} }

bool UtilFileExists( ccptr Filename) {
  static struct stat sb;
  return (stat( DynamicFilename( Filename), &sb) == 0) ? true : false;
}

void UtilFileDelete( ccptr Filename) {
  remove( DynamicFilename( Filename));
}

// Return a volatile value read from a file.

ccptr UtilFileValue( ccptr Filename) {
  static char Value[128];
  if (fptr f = fopen( DynamicFilename( Filename), "r")) {
    if (int n = fread( Value, 1, sizeof( Value)-2, f)) {
      Value[n] = 0;
      fclose( f);
      return UtilTrim( Value);
    }
    fclose( f);
  }
  return "";
}

bool UtilFileSet( ccptr Flagname, ccptr Value) {
  ccptr Current = UtilFileValue( Flagname);
  if (UtilStrCmp( Current, Value) == 0)
  	return false;

  if (fptr f = fopen( DynamicFilename( Flagname), "w")) {
    fprintf( f, "%s\n", Value);
    fclose( f);
  }
  return true;
}

void UtilFlagForce( ccptr Flagname) {
  if (fptr f = fopen( DynamicFilename( Flagname), "w")) {
      fprintf( f, "1\n");
      fclose( f);
} }

void UtilFlagSet( ccptr Flagname) {
  if (!UtilFileExists( Flagname)) {
      if (fptr f = fopen( DynamicFilename( Flagname), "w")) {
          fprintf( f, "1\n");
          fclose( f);
} } }

uint32_t UtilHexToBit32( ccptr Hex) {
  uint32_t v = 0;
  while (*Hex && (*Hex == ' ')) {
    Hex++;
  }
  while (char c = *Hex++) {
    if (strchr( "0123456789", c)) { v <<= 4; v |=      c - '0'; } else
    if (strchr( "abcdef"    , c)) { v <<= 4; v |= 10 + c - 'a'; } else
    if (strchr( "ABCDEF"    , c)) { v <<= 4; v |= 10 + c - 'A'; } else
      return v;
  }
  return v;
}

static timespec TimerStart;
static timespec TimerCheck;

static bool TimerOk = false;

void UtilTimerReset( void) {
  int rc = clock_gettime( CLOCK_REALTIME, &TimerStart);
  if (rc == 0) {
    TimerOk = true;
  } else {
    TimerOk = false;
    ErrorLogLn( "UtilTimerReset clock_gettime rc %d", rc);
} }

// Return elapsed time in mS since the last call to TimerReset, up to
// a limit of 30 seconds.

uint32_t UtilTimer_mS( void) {
  int mS = 0;
  if (TimerOk) {
    if (clock_gettime( CLOCK_REALTIME, &TimerCheck) == 0) {
      uint32_t s = TimerCheck.tv_sec - TimerStart.tv_sec;
      if (TimerCheck.tv_nsec < TimerStart.tv_nsec) {
        TimerCheck.tv_nsec += 1000000000;
        s -= 1;
      }
      uint32_t ns = TimerCheck.tv_nsec - TimerStart.tv_nsec;
      if (s < 30) {
        mS = (s * 1000) + (ns / 1000000);
      } else {
        mS = 30000;
  } } }
  return mS;
}

uint8_t UtilBits( uint8_t Value) {
  uint8_t Bits = 0;
  for (uint8_t Mask = 0x80; Mask; Mask >>= 1) {
    if (Mask & Value) Bits++;
  }
  return Bits;
}

ccptr UtilDateTime( void) {
  char x[32];
  time_t t = time( NULL);
  UtilStrCpy( x, ctime( &t), sizeof( x)); // 'Wed Jun 07 01:00:00 1995'
  x[7] = 0;
  static char y[20] = "YYYYMMDDHHMMSS";
  static ccptr z = "JanFebMarAprMayJunJulAugSepOctNovDec";
  sprintf( y, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",
                 UtilStr2Int( x+20), ((strstr( z, x+4) - z) / 3) + 1, 
                 UtilStr2Int( x+ 8), 
                 UtilStr2Int( x+11), 
                 UtilStr2Int( x+14), 
                 UtilStr2Int( x+17));
  return TempDup( y);
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
