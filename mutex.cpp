//--------------------------------------------------------------------------
// Module: Mutex Support                                      +-----------+
// Author: Michael Nagy                                       | mutex.cpp |
// Date:   14-Aug-2012                                        +-----------+
//--------------------------------------------------------------------------

#include "main.h"

static sem_t * Sem = NULL;

// Obtain a semaphore for a device simple name or derived from a device path
// like /dev/ttyUSB0.  Try for up to 10 seconds if necessary before failing.

int MutexObtain( ccptr NameOrDevPath) {
  if (Sem) {
    ErrorLogLn( "MutexObtain('%s') - semaphore already open", NameOrDevPath);
    return 1; // only one semaphore at a time!
  }
  ccptr SemName = rindex( NameOrDevPath, '/');
  if (!SemName) {
    SemName = NameOrDevPath;
  }
  if (strlen( SemName) < 4) {
    ErrorLogLn( "MutexObtain('%s') - name too short", NameOrDevPath);
    return 2; // device name is too short
  }
  struct timespec SemTs;
  if (clock_gettime( CLOCK_REALTIME, &SemTs)){
    ErrorLogLn( "MutexObtain('%s') - error %d setting timeout", NameOrDevPath, errno);
    return 3;
  }
  SemTs.tv_sec += 2;
  Sem = sem_open( SemName, O_CREAT, S_IRUSR | S_IWUSR, 1);
  if (Sem == SEM_FAILED) {
    ErrorLogLn( "MutexObtain('%s') - error %d opening lock '%s'", NameOrDevPath, errno, SemName);
    return 4;
  }
  if (sem_timedwait( Sem, &SemTs)) {
    if (errno == ETIMEDOUT) {
      ErrorLogLn( "MutexObtain('%s') - timed out waiting for lock", NameOrDevPath);
      int SemVal;
      sem_getvalue( Sem, &SemVal);
      ErrorLogLn( "MutexObtain('%s') - forcing semaphore reset, value was: %d", NameOrDevPath, SemVal);
      sem_unlink( SemName);
      sem_close( Sem);
      return 5;
    }
    ErrorLogLn( "MutexObtain('%s') - could not get lock", NameOrDevPath);
    return 6;
  }
  return 0;
}

// Release the semaphore obtained above.

void MutexRelease( void) {
  if (Sem) {
    sem_post( Sem);
    Sem = NULL;
} }

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

