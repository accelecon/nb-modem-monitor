//--------------------------------------------------------------------------
// Module: Modem Interface                                    +-----------+
// Author: Michael Nagy                                       | modem.cpp |
// Date:   15-Aug-2012                                        +-----------+
//--------------------------------------------------------------------------

#include "main.h"
#include <sys/types.h>
#include <sys/wait.h>

// Assemble a response line from the modem.  Lines end with line-feeds.
// Treat all other unprintable characters as blanks.  Never append a
// blank to an empty line or a line that already ends with a blank.
// Also discard trailing blanks.

static ccptr GetLine( int Port, int TimeoutMs = 100) {
  static uint8_t Line[900];
  int n = 0;
  while (n < 118) {
    if (SerialGet( Port, Line[n], TimeoutMs)) {
      if (Line[n] == 0x0a) {
        break; // line-feed ends line
      } else {
        if (!(Line[n] & 0xE0)) {
          Line[n] = ' '; // treat all other unprintables as blanks
        }
        if ((Line[n] != ' ') || (n && Line[n-1] != ' ')) {
          n++; // accept printable character or non-initial isolated blank
      } }
    } else {
      if (n) {
        break; // timeout ends line
      }
      return NULL; // timeout on empty line aborts
  } }
  Line[n] = 0;
  if (n) {
    if (Line[n-1] == ' ') {
      Line[n-1] = 0; // whack any trailing blank
    }
    return TempDup( (cptr) Line); // return non-blank line
  }
  return GetLine( Port, TimeoutMs); // ignore blank line and return next line
}

// Send an AT command to the modem (with pacing enabled).

static bool PutLine( int Port, ccptr Command) {
  bool rc = true;
  if (!SerialPutString( Port, "AT"   , SERIALPUT_PACE)) rc = false;
  if (!SerialPutString( Port, Command, SERIALPUT_PACE)) rc = false;
  if (!SerialPutString( Port, "\r"   , SERIALPUT_PACE)) rc = false;
  return rc;
}

// Issue a command to a modem and capture its (possibly mult-line) response.
// Responses should end with OK, ERROR or NO CARRIER.  Only return a captured
// set of response lines if we got an OK.  Use a 300 mS timeout for the
// first returned line only, 100 mS for subsequent lines.

static Reply_t ModemQueryReply( int Port, ccptr Query) {
  tcflush( Port, TCIOFLUSH);
  if (PutLine( Port, Query)) {
    bool Ok = false;
    Reply_t Reply = (Reply_t) calloc( 22, sizeof( ccptr));
    int Lines = 0;
    while (Lines < 20) {
      if ((Reply[Lines] = GetLine( Port, Lines ? 300 : 500))) {
        if (Lines || (UtilStrCmp( Reply[Lines], "AT") &&
                      UtilStrCmp( Reply[Lines], "OK"))) {
          if (UtilStrCmp( Reply[Lines], "OK"                 ) == 0) { Ok = true; break; }
          if (UtilStrCmp( Reply[Lines], "ERROR"              ) == 0)              break;
          if (UtilStrCmp( Reply[Lines], "NO CARRIER"         ) == 0)              break;
          if (UtilStrCmp( Reply[Lines], "COMMAND NOT SUPPORT") == 0)              break;
          Lines++;
        }
      } else {
        break;
    } }
    DebugLogLn( "Query: 'AT%s'", Query);
    for (int n = 0; Reply[n]; n++) {
      DebugLogLn( "  Reply: '%s'", Reply[n]);
    }
    if (Ok == true) {
      return Reply;
    } else {
      free( Reply);
      return NULL;
    }
  }
  ErrorLogLn( "PutLine(%d,'%s') failed", Port, Query);
  return NULL;
}

// Open a modem port.  Set Handle to a non-zero value on success.

static bool ModemOpen( int & Handle, ccptr Path) {
  if (SerialOpen( Handle, Path, SERIALOPEN_COOKED)) {
    return true;
  }
  return false;
}

// Close a modem port.  Set Handle to zero.

static bool ModemClose( int & Handle) {
  return SerialClose( Handle);
}

// Supply a serial port, a modem command (the AT prefix is implicit) and a pointer
// to an array of ASCIIZ strings (should be NULL initially).  Returns the result,
// or nothing if the string ERROR is returned by the serial device.

bool ModemQueryAlloc( Reply_t & Reply, ccptr UsbPort, ccptr Command) {
  ModemQueryFree( Reply);
  if (int rc = MutexObtain( UsbPort)) {
    ErrorLogLn( "MutexObtain('%s') rc %d", UsbPort, rc);
  } else {
    int Modem = 0;
    if (ModemOpen( Modem, UsbPort)) {
      Reply = ModemQueryReply( Modem, Command);
      ModemClose( Modem);
    } else {
      ErrorLogLn( "ModemOpen('%s')", UsbPort);
    }
    MutexRelease();
  }
  return Reply ? true : false;
}

// Supply a WDM port, a shell command and a pointer
// to an array of ASCIIZ strings (should be NULL initially).  Returns the result.

bool ModemQmiQueryAlloc( Reply_t & Reply, ccptr WdmPort, ccptr Command){
  char cmd[500];
  FILE * QmiStdOut;
  ModemQueryFree( Reply);
  snprintf(cmd, 500, "qmicli -p -d %s %s 2> /dev/null", WdmPort, Command);
  DebugLogLn("Running: %s", cmd);
  QmiStdOut = popen( cmd , "r");
  int n = 0;
  if(QmiStdOut){
    Reply = (Reply_t) calloc( 101, sizeof( ccptr));
    static char Line[900];
    while (n < 100) {
      if (fgets(Line, 900, QmiStdOut)){
        Reply[n] = TempDup(Line);
        DebugLogLn(Reply[n]);
      } else {
        break;
      }
      n++;
    }
    if (n >= 100) {
      /* we need to read teh rest of the output */
      while (fgets(Line, 900, QmiStdOut))
	;
    }
  }
  int QmiRetVal = pclose(QmiStdOut);
  DebugLogLn("qmicli return value: {%d}", QmiRetVal);
  if (QmiRetVal == -1)
    return false;
  QmiRetVal = WIFEXITED(QmiRetVal) ? WEXITSTATUS(QmiRetVal) : -1;
  return (QmiRetVal == 0) ? true : false;
}

// Free a pointer to an array of ASCIIZ strings.

void ModemQueryFree( Reply_t & Reply) {
  UtilReplyFree( Reply);
}

// Look for the first instance of Key followed by a colon and a space
// in the block of Text lines and return the following Value token.  By
// default, Values end on the first space.  If the ToEndOfLine flag is
// set, then Value gets everything till the end of the line.  If the final
// value is quoted, the quotes are removed before return.

ccptr ModemGetValue( Reply_t Text, ccptr Key, bool ToEndOfLine) {
  if (Text) {
    static char Key2[80];
    sprintf( Key2, "%s: ", Key);
    while (ccptr Line = *Text++) {
      if (ccptr Hit = strstr( Line, Key2)) {
        static char Value[80];
        int n = 0;
        Hit += strlen( Key2);
        for (n = 0; Value[n] = *Hit++; n++) { // tokens end on end-of-line
          if ((ToEndOfLine == false) && (Value[n] == ' ')) {
            break; // tokens end on a space
        } }
        Value[n] = 0;
        if (n) {
          if ((n > 2) && (Value[0] == '"') && (Value[n-1] == '"')) {
            Value[n-1] = 0;
            return TempDup( Value+1);
          }
          return TempDup( Value);
    } } }
    return "";
  }
  return "Unknown";
}

// Return for the Index'th instance (one-based index) of a Value in a
// comma-delimited compound value corresponding to the specified Key.  If
// it is quoted, strip the quotes.  Return "" for no value found or
// empty value found.

ccptr ModemGetValueIndex( Reply_t Text, ccptr Key, int Index) {
  if (Text) {
    if (ccptr Values = ModemGetValue( Text, Key, true)) {
      while (Index-- > 1) {                 // skip fields until target field located
        while (*Values && (*Values != ',')) {
          Values++; // skip over current field
        }
        if (*Values != ',' ) {
          return "-1"; // if no more fields, fail
        }
        Values++; // skip over eof comma and advance to next field
      }
      if (cptr Value = (cptr) TempDup( Values)) {     // get a mutable copy of the remaining Values string
        if (cptr Eof = strchr( Value, ',')) { // get a pointer to trailing comma (if not last field)
          *Eof = 0;                           // whack trailing comma (if found)
        }
        if (Value[0] == '"') {                    // if value starts with a quote
          if (cptr Eoq = strchr( Value+1, '"')) { // and trailing quote is also found
            Value++;                              // advance past staring quote
            *Eoq = 0;                             // whack trailing quote
        } }
        while (Value && (Value[0] == ' ')) { // whack leading spaces
          Value++;
        }
        if (int n = strlen( Value)) {
          while (n && (Value[n-1] == ' ')) {
            Value[--n] = 0;
        } }
        if (strlen( Value)) {
          return TempDup( Value); // return final Value string (unquoted if necessary)
  } } } }
  return "-1";
}

// Return any non-blank result from an AT command (not including
// the presumed terminal 'OK').

ccptr ModemGetValueRaw( Reply_t Text) {
  if (Text) {
    while (ccptr Line = *Text++) {
      if (strncmp( Line, "AT", 2) && strncmp( Line, "OK", 2)) {
        while (*Line == ' ') Line++;
        return TempDup( Line);
  } } }
  return "";
}

// Spin through the block of text returned by the +CSS command.  If
// we find a line starting with 1, then we are in the 800 MHz band,
// if it starts with 2, then we are in the 1900 MHz band.

ccptr ModemRemapCssBand( Reply_t Text) {
  if (Text) {
    while (ccptr Line = *Text++) {
      if (strncmp( Line, "1,", 2) == 0) return  "800";
      if (strncmp( Line, "2,", 2) == 0) return "1900";
  } }
  return "";
}

// Spin through the block of text returned by the +CSS command.  We
// should see a line like this
//
//   1,PB,4192,6
//
// The SID is the 4192 in this example, followed by the NID of 6.

ccptr ModemRemapCssSid( Reply_t Text) {
  if (Text) {
    while (ccptr Line = *Text++) {
      if (ccptr x = strchr( Line, ',')) {
        if (x = strchr( x+1, ',')) {
          static char Sid[16];
          UtilStrCpy( Sid, x+1, sizeof( Sid));
          if (cptr y = strchr( Sid, ',')) {
            *y = 0;
            if (strlen( Sid)) {
              return TempDup( Sid);
  } } } } } }
  return "";
}

ccptr ModemRemapCssNid( Reply_t Text) {
  if (Text) {
    while (ccptr Line = *Text++) {
      if (ccptr x = strchr( Line, ',')) {
        if (x = strchr( x+1, ',')) {
          if (x = strchr( x+1, ',')) {
            static char Nid[16];
            UtilStrCpy( Nid, x+1, sizeof( Nid));
            if (strlen( Nid)) {
              return TempDup( Nid);
  } } } } } }
  return "";
}

// Based on the !STATUS command response, we need to return a friendly
// and consistent string indicating the current network technology.  We
// use the 'HDR Revision:' field for this.  Values of this field map
// as follows:
//
//        'A' --> 'EVDO Rev A'
//        '0' --> 'EVDO Rev 0'
//
// This logic only applies to the 'CMDA' modems (verizon/sprint).

ccptr ModemRemapHdrNetwork( cptr StatusHdrRevision) {
  static char Temp[900];
  if (StatusHdrRevision) {
    if (UtilStrCmp( StatusHdrRevision, "A") == 0) return "EVDO Rev A";
    if (UtilStrCmp( StatusHdrRevision, "0") == 0) return "EVDO Rev 0";
    if (UtilStrCmp( StatusHdrRevision, "Unknown")) {
      sprintf( Temp, "1xRTT (%s)", StatusHdrRevision);
    } else {
      UtilStrCpy( Temp, "1xRTT", sizeof( Temp));
    }
    return TempDup( Temp);
  }
  return "";
}

// Based on the *CNTI=0 and !REL? command responses, we need to return
// a friendly and consistent string indicating the current network
// technology.  The *CNTI=0 command returns a string of the following
// form:
//        *CNTI: 0,UMTS
//
// When this command returns HSDPA, we need to look further at the
// !REL? command responses Protocol field to discriminate between the
// UPA on and off states.
//
// This logic only applies to the 'UMTS' modems (att/kpn/vodafone/tmobile/bellca).

ccptr ModemRemapCntiNetwork( cptr Cnti, cptr Cops, cptr RelProtocol) {
  static char Temp[900];
  if (Cnti) {
    if (UtilStrStr( Cnti, "GSM"  )) return "GSM" ;
    if (UtilStrStr( Cnti, "GPRS" )) return "GPRS";
    if (UtilStrStr( Cnti, "EDGE" )) return "EDGE";
    if (UtilStrStr( Cnti, "UMTS" )) return "UMTS";
    if (UtilStrStr( Cnti, "HSDPA")) {
      if (RelProtocol) {
        if (UtilStrCmp( RelProtocol, "Release 5") == 0) return "HSDPA"      ;
        if (UtilStrCmp( RelProtocol, "Release 6") == 0) return "HSDPA/HSUPA";
        sprintf( Temp, "HSDPA (%s)", RelProtocol);
        return TempDup( Temp);
  } } }
  if (Cops) {
    if (UtilStrCmp( Cops, "0")) return "GSM"        ;
    if (UtilStrCmp( Cops, "1")) return "GSM-C"      ;
    if (UtilStrCmp( Cops, "2")) return "UTRAN"      ;
    if (UtilStrCmp( Cops, "3")) return "EDGE"       ;
    if (UtilStrCmp( Cops, "4")) return "HSDPA"      ;
    if (UtilStrCmp( Cops, "5")) return "HSUPA"      ;
    if (UtilStrCmp( Cops, "6")) return "HSDPA/HSUPA";
    if (UtilStrCmp( Cops, "7")) return "E-UTRAN"    ;
  }
  if (Cnti) {
    if (UtilStrCmp( Cnti, "Unknown")) {
      sprintf( Temp, "Unknown (C='%s')", Cnti);
    } else {
      UtilStrCpy( Temp, "Unknown", sizeof( Temp));
    }
    return TempDup( Temp);
  } else {
    if (RelProtocol) {
      sprintf( Temp, "UNKNOWN (R='%s')", RelProtocol);
      return TempDup( Temp);
  } }
  return "";
}

// Try to weasle the LAC and/or CID out of a GSM modem.  Assume it
// is returned in hex, so convert to decimal.

void ModemGetLacCidGsm( Reply_t Creg, Reply_t Cgreg, ccptr & Lac, ccptr & Cid) {
  if (Lac = ModemGetValueIndex( Creg, "+CREG", 3)) {
      Cid = ModemGetValueIndex( Creg, "+CREG", 4);
  } else {
    if (Lac = ModemGetValueIndex( Cgreg, "+CGREG", 3)) {
        Cid = ModemGetValueIndex( Cgreg, "+CGREG", 4);
  } }
  char Buffer[32];
  if (Lac) {
    sprintf( Buffer, "%u", UtilHexToBit32( Lac));
    Lac = TempDup( Buffer);
  }
  if (Cid) {
    sprintf( Buffer, "%u", UtilHexToBit32( Cid));
    Cid = TempDup( Buffer);
} }

bool ModemFindString( Reply_t Response, ccptr Target) {
  if (Response) {
    while (ccptr x = *Response++) {
      if (UtilStrStr( x, Target)) return true;
  } }
  return false;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

