//--------------------------------------------------------------------------
// Module: Serial Driver Interface                             +----------+
// Author: Michael Nagy                                        | serial.h |
// Date:   09-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

#define SERIALOPEN_RAW     0x00
#define SERIALOPEN_COOKED  0x01

#define SERIALOPEN_115200  0x00
#define SERIALOPEN_9600    0x02

bool SerialOpen( int & PortHandle, ccptr SerialDevice, int OpenOptions = 0);

#define SERIALPUT_PACE false

bool SerialPut( int PortHandle, uint8_t Data, bool Pace = false);
bool SerialPutBuffer( int PortHandle, bptr Data, int Count, bool Pace = false);
bool SerialPutString( int PortHandle, ccptr Data, bool Pace = false);

bool SerialGet( int PortHandle, uint8_t & Data, int TimeoutMs);
bool SerialClose( int & PortHandle);

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
